import com.amo.websocket.api.Endpoint;
import com.amo.websocket.api.Session;
import com.amo.websocket.server.BasicContainer;

import javax.websocket.CloseReason;
import java.io.IOException;
import java.net.URISyntaxException;

/*
 * This Java source file was generated by the Gradle 'init' task.
 */
public class App {
    public String getGreeting() {
        return "Hello world.";
    }

    public static void main(String[] args) throws URISyntaxException, InterruptedException, IOException {
        BasicContainer bc = new BasicContainer();
        bc.registerEndpoint("/", new Server());
        bc.listen(8082);
        System.in.read();
    }

    static class Server implements Endpoint {

        private Session session;

        @Override
        public void onConnect(Session session) {
            this.session = session;
            System.out.println("Received a connecton. Session id: "+ session.getSessionId());
        }

        @Override
        public void onTextMessage(String data) {
            System.out.println("Received text " + data);
            session.getWebsocketHandler().sendMessage(data.toUpperCase());
        }

        @Override
        public void onBinaryMessage(byte[] data) {
            System.out.println("Received binary "+ data);
        }

        @Override
        public void onError(Throwable throwable) {
            throwable.printStackTrace();
        }

        @Override
        public void onClose(CloseReason closeReason) {
            System.out.println("Received close: "+ closeReason.getCloseCode());
        }
    }
}
