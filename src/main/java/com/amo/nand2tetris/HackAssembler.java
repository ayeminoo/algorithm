package com.amo.nand2tetris;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ayeminoo on 6/21/17.
 */
public class HackAssembler {
    private final String ALL_ZERO = "0000000000000000";
    private static Map<String, String> compMap = new HashMap<>();
    private static Map<String, String> destMap = new HashMap<>();
    private static Map<String, String> jumpMap = new HashMap<>();
    private String EXTENSION = ".asm";
    private String OUTPUT_EXTENSION = ".hack";
    static {
        //comp start with 0
        compMap.put("0", "0101010");
        compMap.put("1", "0111111");
        compMap.put("-1", "0111010");
        compMap.put("D", "0001100");
        compMap.put("A", "0110000");
        compMap.put("!D", "0001101");
        compMap.put("-A", "0110011");
        compMap.put("D+1", "0011111");
        compMap.put("A+1", "0110111");
        compMap.put("D-1", "0001110");
        compMap.put("A-1", "0110010");
        compMap.put("D+A", "0000010");
        compMap.put("D-A", "0010011");
        compMap.put("A-D", "0000111");
        compMap.put("D&A", "0000000");
        compMap.put("D|A", "0010101");
        //comp start with 1
        compMap.put("M", "1110000");
        compMap.put("!M", "1110001");
        compMap.put("-M", "1110011");
        compMap.put("M+1", "1110111");
        compMap.put("M-1", "1110010");
        compMap.put("D+M", "1000010");
        compMap.put("D-M", "1010011");
        compMap.put("M-D", "1000111");
        compMap.put("D&M", "1000000");
        compMap.put("D|M", "1010101");

        //initializing destMap
        destMap.put("", "000");
        destMap.put(null, "000");
        destMap.put("M", "001");
        destMap.put("D", "010");
        destMap.put("MD", "011");
        destMap.put("A", "100");
        destMap.put("AM", "101");
        destMap.put("AD", "110");
        destMap.put("AMD", "111");

        //initializing destMap
        jumpMap.put("", "000");
        jumpMap.put(null, "000");
        jumpMap.put("JGT", "001");
        jumpMap.put("JEQ", "010");
        jumpMap.put("JGE", "011");
        jumpMap.put("JLT", "100");
        jumpMap.put("JNE", "101");
        jumpMap.put("JLE", "110");
        jumpMap.put("JMP", "111");
    }

    public static void main(String[]args) throws Exception {
        if(args==null || args.length == 0) {
            System.err.println("Usage: HackAssembler program");
            return;
        }
        HackAssembler hackAssembler = new HackAssembler();
        hackAssembler.assemble(args[0]);

//        HackAssembler hackAssembler = new HackAssembler();
//        System.out.println(hackAssembler.translateCInstruction("0;JMP"));
    }

    public void assemble(String filePath) throws Exception {
        checkFileExtension(filePath);

        BufferedReader in = new BufferedReader(new FileReader(filePath));
        BufferedWriter out = new BufferedWriter(new FileWriter(filePath.substring(0, filePath.length()-EXTENSION.length()) + OUTPUT_EXTENSION));
        String line;
        while((line = in.readLine()) != null){
            line = translateToMachineCode(line);
            if(line == null) continue;
            out.write(line+"\n");
        }
        in.close();
        out.close();
    }

    public void checkFileExtension(String path) throws Exception {
        if(path == null || path.length()<=EXTENSION.length()+1 ) throw new Exception("extension not valid. use {name}" + EXTENSION);
        for(int i = path.length()-1; i>=path.length() - EXTENSION.length();i--){
            if(path.charAt(i) != EXTENSION.charAt(i - (path.length()- EXTENSION.length()))){
                throw new Exception("extension not valid. use {name}" + EXTENSION);
            }
        }
    }
    /**
     * Translate every line of code into machine.
     * If the line is white space, it will return null.
     * @param line
     * @return
     */
    public String translateToMachineCode(String line){
        int index = line.indexOf("//");
        if(index!=-1){
            //remove white-space and comments
            line = line.substring(0, index);
        }
        line = line.replaceAll("\\s+", "");
        //empty = white-spaced line, so ignore
        if(line.length()==0) return null;
        if(line.charAt(0) == '@'){
            //handle A instruction
            return translateAInstruction(line);
        }else{
            //handle C instruction
            return translateCInstruction(line);
        }
    }

    private String translateAInstruction(String aInstruction){
        String binaryString =  Integer.toBinaryString(Integer.parseInt(aInstruction.replace('@','0')));
        return ALL_ZERO.substring(0, 16-binaryString.length()) + binaryString;
    }

    private String translateCInstruction(String bInstruction){
        StringBuffer temp = new StringBuffer();
        String dest = null;
        String comp = null;
        String jump = null;
        for(int i=0; i<bInstruction.length();i++){
            switch(bInstruction.charAt(i)){
                case ' ': continue;
                case '=':
                    dest = temp.toString();
                    temp.setLength(0);
                    break;
                case ';':
                    comp = temp.toString();
                    temp.setLength(0);
                    break;
                default:
                    temp.append(bInstruction.charAt(i));
            }
        }
        if(comp==null) comp = temp.toString();
        else jump = temp.toString();

        return "111"+ compMap.get(comp) + destMap.get(dest) + jumpMap.get(jump);
    }
}
