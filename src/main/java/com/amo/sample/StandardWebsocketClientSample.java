package com.amo.sample;

import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.client.WebSocketConnectionManager;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;

/**
 * Created by ayeminoo on 1/4/18.
 */
public class StandardWebsocketClientSample {
    public static void main2(String[]args) throws IOException {


        StandardWebSocketClient simpleWebSocketClient = new StandardWebSocketClient();
        CustomTextWebsocketMessageHandler websocketHandler = new CustomTextWebsocketMessageHandler(){
            @Override
            public void sendMessage(TextMessage message) throws IOException {
                session.sendMessage(message);
            }

            private WebSocketSession session;

            @Override
            public void afterConnectionEstablished(WebSocketSession ses) throws Exception {
                session = ses;
                System.out.println("connection established");
                sendMessage(new TextMessage("ok ok testing"));
            }

            @Override
            protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
                System.out.println("received: " + message.getPayload());
            }
        };

        WebSocketConnectionManager manager = new WebSocketConnectionManager(simpleWebSocketClient, websocketHandler, "ws://echo.websocket.org/");

        manager.start();

//        websocketHandler.sendMessage(new TextMessage("testing testing"));

        System.in.read();
    }
}


abstract class CustomTextWebsocketMessageHandler extends TextWebSocketHandler {
    public abstract void sendMessage(TextMessage message) throws IOException;

}
