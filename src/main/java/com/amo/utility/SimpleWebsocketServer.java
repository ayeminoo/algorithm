//package com.amo.utility;
//
//import com.amo.websocket.Frame;
//import com.amo.websocket.FrameReader;
//import com.amo.websocket.FrameWriter;
//import com.amo.websocket.HandshakeHandler;
//import com.amo.websocket.WebsocketHandler;
//import com.amo.websocket.server.BasicFrameReader;
//import com.amo.websocket.server.BasicFrameWriter;
//import com.amo.websocket.server.BasicHandshakeHandler;
//import com.amo.websocket.server.BasicWebsocketHandler;
//
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.OutputStream;
//import java.net.ServerSocket;
//import java.net.Socket;
//import java.security.NoSuchAlgorithmException;
//
///**
// * Created by ayeminoo on 1/2/18.
// */
//public class SimpleWebsocketServer {
//
//    private WebsocketHandler webSocketHandler;
//    private HandshakeHandler handshakeHandler;
//    private int port = 8080;
//    public SimpleWebsocketServer(){}
//    public SimpleWebsocketServer(int port) throws IOException {
//        port = 80;
//    }
//
//    public void start() throws IOException {
//        ServerSocket server = new ServerSocket(8080);
//
//        System.out.println("Server has started on 127.0.0.1:80.\r\nWaiting for a connection...");
//
//        Socket client = server.accept();
//        System.out.println("A client connected.");
//        InputStream in = client.getInputStream();
//        OutputStream out = client.getOutputStream();
//
//        handshakeHandler.doHandshake(in, out);
//        webSocketHandler.onConnect(new BasicFrameWriter(out));
//        FrameReader frameReader = new BasicFrameReader(in);
//        FrameWriter frameWriter = null;// assign frame writer
//        new Thread()
//        {
//            public void run() {
//                while(true){
//                    try {
//                        Frame frame = frameReader.read();
//                        webSocketHandler.onReceive(frame);
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                        break;
//                    }
//                }
//            }
//        }.start();
//    }
//
//    public void setHandshakeHandler(HandshakeHandler handshakeHandler) {
//        this.handshakeHandler = handshakeHandler;
//    }
//
//    //todo: add multiple endpoint support
//    public void addWebsocketHandler(String endpoint, WebsocketHandler websocketHandler) {
//
//    }
//
//    public void addWebsocketHandler(WebsocketHandler webSocketHandler) {
//        this.webSocketHandler = webSocketHandler;
//    }
//
//    public static void main(String[] args) throws IOException, NoSuchAlgorithmException {
//        SimpleWebsocketServer server = new SimpleWebsocketServer(8080);
//        server.addWebsocketHandler(new BasicWebsocketHandler());
//        server.setHandshakeHandler(new BasicHandshakeHandler());
//        server.start();
//    }
//
//}
//
//
