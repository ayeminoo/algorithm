package com.amo.codility;

import java.util.Arrays;

/**
 * Created by ayeminoo on 3/11/17.
 */
public class CyclicRotation {
    public int[] solution(int[] input, int k) {
        // write your code in Java SE 8
        if (input.length == 0) return input;

        int result[] = new int[input.length];
        for(int i = 0; i < input.length ; i++){
            int newIndex = (i + k) % input.length ;
            result[newIndex] = input[i];
        }
        return result;
    }


    public static void main(String[]args){
        System.out.println(Arrays.toString(new CyclicRotation().solution(new int[]{3, 8, 9, 7, 6}, 3)));
    }
}
