package com.amo.codility;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Created by ayeminoo on 3/13/17.
 */
public class MapLooping {
    public static void main(String[]args){
        Map<Integer, String> map = new HashMap<>();
        map.put(1, "testing");
        map.put(2, "testing2");

        //looping using keyset
        for(Integer i: map.keySet()){
            System.out.println( "key: "+ i);
            System.out.println( "Value: "+ map.get(i));
        }

        //using iterator & keyset
        Set<Integer> keySet = map.keySet();
        Iterator<Integer> iterator = keySet.iterator();
        while(iterator.hasNext()){
            Integer key = iterator.next();
            System.out.println( "key: "+ key);
            System.out.println( "Value: "+ map.get(key));
        }

        //using entrySet
        for(Map.Entry<Integer, String> entry: map.entrySet()){
            System.out.println( "key: "+ entry.getKey());
            System.out.println( "Value: "+ entry.getValue());
        }

        //using iterator and entrySet
        Iterator<Map.Entry<Integer, String>> iterator1 = map.entrySet().iterator();
        while(iterator1.hasNext()){
            Map.Entry<Integer, String> entry = iterator1.next();
            System.out.println("Key: "+ entry.getKey());
            System.out.println("Value: "+ entry.getValue());
        }

    }
}
