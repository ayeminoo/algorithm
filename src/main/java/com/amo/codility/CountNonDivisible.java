package com.amo.codility;

import com.amo.codility.utils.ArrayUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * You are given a non-empty zero-indexed array A consisting of N integers.

 For each number A[i] such that 0 ≤ i < N, we want to count the number of elements of the array that are not the divisors of A[i]. We say that these elements are non-divisors.

 For example, consider integer N = 5 and array A such that:

 A[0] = 3
 A[1] = 1
 A[2] = 2
 A[3] = 3
 A[4] = 6
 For the following elements:

 A[0] = 3, the non-divisors are: 2, 6,
 A[1] = 1, the non-divisors are: 3, 2, 3, 6,
 A[2] = 2, the non-divisors are: 3, 3, 6,
 A[3] = 3, the non-divisors are: 2, 6,
 A[4] = 6, there aren't any non-divisors.
 Write a function:

 class Solution { public int[] solution(int[] A); }
 that, given a non-empty zero-indexed array A consisting of N integers, returns a sequence of integers representing the amount of non-divisors.

 The sequence should be returned as:

 a structure Results (in C), or
 a vector of integers (in C++), or
 a record Results (in Pascal), or
 an array of integers (in any other programming language).
 For example, given:

 A[0] = 3
 A[1] = 1
 A[2] = 2
 A[3] = 3
 A[4] = 6
 the function should return [2, 4, 3, 2, 0], as explained above.

 Assume that:

 N is an integer within the range [1..50,000];
 each element of array A is an integer within the range [1..2 * N].
 Complexity:

 expected worst-case time complexity is O(N*log(N));
 expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
 Elements of input arrays can be modified.

 * Created by ayeminoo on 5/9/17.
 */
public class CountNonDivisible {

    public int[] solution(int[] A)
    {
        int N = A.length;
        int max = N * 2 + 1;

        int[] c = new int[max + 1];
        for (int i = 0; i < N; i++)
        {
            c[A[i]]++;
        }

        int[] result = new int[N];
        for (int i = 0; i < N; i++)
        {
            int r = 0;
            int x = A[i];
            int target = (int)Math.sqrt(x);
            for (int d = 1; d <= target; d++)
            {
                if (x % d == 0)
                {
                    r += c[d];
                    if (x / d != d)
                    {
                        r += c[x / d];
                    }
                }
            }
            result[i] = N - r;
        }

        return result;
    }

    //this is wrong solution because it is not handling the [23, 24, 28, 11, 1, 24, 21, 24, 20, 23, 26, 14, 2, 8, 14]
    //array properly
    public int[] solution2(int[] a){

        int [] seives = ArrayUtils.getSeive(a.length* 2);
        Map<Integer, Integer> countStore = new HashMap<>();

        for(int i: a){
            countStore.put(i, countStore.get(i)==null?1:
            countStore.get(i)+1);
        }

        int[]sol = new int[a.length];

        for(int i=0; i < a.length ;i++){
            Map<Integer, Boolean> used = new HashMap<>();
            Integer count = new Integer(0);
            if(a[i] == 1){
                count += countStore.get(1) == null? 0: countStore.get(1);
                sol[i] = a.length - count ;
                continue;
            }
            count += countStore.get(1) == null? 0: countStore.get(1);
            used.put(1,true);
            int index = a[i];

            while(true){
                if(used.get(index) == null){
                    count += countStore.get(index) == null? 0: countStore.get(index);
                    used.put(index, true);
                }
                if(seives[index] == 0)break;
                if(used.get(seives[index]) == null){
                    count += countStore.get(seives[index]) == null? 0: countStore.get(seives[index]);
                    used.put(seives[index], true);
                }
                index = index / seives[index];
            }
            sol[i] = a.length - count ;
        }
        return sol;
    }

    public static void main(String[]args) {
        CountNonDivisible me = new CountNonDivisible();

        System.out.println(Arrays.toString(new CountNonDivisible().solution2(new int[]{23, 24, 28, 11, 1, 24, 21, 24, 20, 23, 26, 14, 2, 8, 14})));
        System.out.println(Arrays.toString(new CountNonDivisible().solution(new int[]{1, 2})));
        System.out.println(Arrays.toString(new CountNonDivisible().solution(new int[]{2, 4, 6, 8, 10})));

        java.util.Random r = new java.util.Random();
        int[] kickerNumbers = r.ints(1, 30).limit(15).toArray();

        System.out.println(Arrays.equals(me.solution(new int[]{3,1,2,3,6}), (me.solution2(new int[]{3,1,2,3,6}))));

        System.out.println(Arrays.equals(me.solution(kickerNumbers), (me.solution2(kickerNumbers))));

        System.out.println(Arrays.toString(kickerNumbers));
        System.out.println(Arrays.toString(me.solution(kickerNumbers)));
        System.out.println(Arrays.toString(me.solution2(kickerNumbers)));

    }
}
