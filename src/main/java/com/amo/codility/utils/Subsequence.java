package com.amo.codility.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ayeminoo on 5/26/17.
 */
public class Subsequence {
    private BinaryArithmetic binaryArithmetic = new BinaryArithmetic();


    public List<String> getAllSubsequence(String sequence){
        List<String> subsequences = new ArrayList<>();
        subsequences.add(null);
        int[]binary = new int[sequence.length()];
        for(int i = 1; i< Math.pow(2, sequence.length()); i++){
            binaryArithmetic.addOne(binary);
            subsequences.add(convert(binary, sequence));
        }
        return subsequences;
    }

    /**
     * Given two strings S and T, find length of the shortest subsequence in S which is not a subsequence in T. If no such subsequence is possible, return -1. A subsequence is a sequence that appears in the same relative order, but not necessarily contiguous. A string of length n has 2^n different possible subsequences.

     String S of length m (1 <= m <= 1000)
     String T of length n (1 <= n <= 1000)
     * @param firstSequence
     * @param secondSequence
     * @return
     */
    public int findShortestUncommonSequence(char[]firstSequence, char[] secondSequence){
        int ans = getShortestUncommonSequence(firstSequence, secondSequence);
        if (ans >= MAX)
            ans = -1;
        return ans;
    }

    static int MAX = 1005;
    private int getShortestUncommonSequence(char[] s, char[]t){
        if(s.length == 0) return MAX;
        if(t.length == 0) return 1;
        int i = 0;
        for(;i<t.length;i++){
            if(t[i] == s[0])break;
        }

        if(i == t.length){ // not found
            return 1;
        }

        //found
        return Math.min(1 + getShortestUncommonSequence(Arrays.copyOfRange(s, 1, s.length),
                Arrays.copyOfRange(t, i +1, t.length)), getShortestUncommonSequence(Arrays.copyOfRange(s, 1, s.length), t));
    }

    public int findShortestUncommonSequenceUsingLoop(char[]firstSequence, char[]secondSequence){
        int min = 1005;
        for(int i = 0; i < firstSequence.length; i++){
            int count = 0;
            boolean found= false;
            int ti = i;
            for(int j= 0; j< secondSequence.length; j++){
                if(firstSequence[ti] == secondSequence[j]){
                    count++;
                    ti++;
                }
            }
            if(found == false) return 1;
            min = Math.min(min, count);
        }
        return min==1005? -1: min;
    }


    public int findLengthOfLongestCommonSequence(char[]firstSequence, char[]secondSequence){
        if(firstSequence == null || secondSequence == null
                || firstSequence.length == 0 || secondSequence.length == 0) return 0;
        int i= 0;
        for(; i<secondSequence.length; i++){
            if(secondSequence[i] == firstSequence[0])break;
        }
        if(i == secondSequence.length) return findLengthOfLongestCommonSequence(Arrays.copyOfRange(firstSequence, 1, firstSequence.length),
                secondSequence);

        return Math.max(1 + findLengthOfLongestCommonSequence(Arrays.copyOfRange(firstSequence, 1, firstSequence.length),
                Arrays.copyOfRange(secondSequence, i +1, secondSequence.length)),
                findLengthOfLongestCommonSequence(Arrays.copyOfRange(firstSequence, 1, firstSequence.length),
                secondSequence));
    }

    private String convert(int[]binary, String sequence){
        StringBuilder stringBuilder = new StringBuilder();
        for(int i= 0; i< binary.length; i++){
            if(binary[i] == 1) stringBuilder.append(sequence.charAt(i));
        }
        return stringBuilder.toString();
    }
}
