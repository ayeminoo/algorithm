package com.amo.codility.utils;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by ayeminoo on 5/26/17.
 */
public class StringUtilsTest {
    @Test
    public void testRandomString() throws Exception {
        assertEquals(10, StringUtils.randomString(10).length());
    }

}