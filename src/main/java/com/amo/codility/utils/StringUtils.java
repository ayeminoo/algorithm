package com.amo.codility.utils;

import java.util.Random;

/**
 * Created by ayeminoo on 5/26/17.
 */
public class StringUtils {
    private static String alphaNumeric = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

    public static String randomString(int length){
        Random random = new Random();
        StringBuilder builder = new StringBuilder();
        for(int i=0; i< length;i++){
            builder.append(alphaNumeric.charAt((int)(random.nextFloat() * alphaNumeric.length())));
        }
        return builder.toString();
    }
}
