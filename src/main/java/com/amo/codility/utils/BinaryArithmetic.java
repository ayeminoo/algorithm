package com.amo.codility.utils;

import java.util.Arrays;

/**
 * Just for fun amateur implementation of binary in int array
 * Created by ayeminoo on 5/27/17.
 */
public class BinaryArithmetic {

    public void addOne(int[]binary){
        add1ToIndex(binary, binary.length-1);
    }

    private static void add1ToIndex(int[]binary, int index){
        if(binary[index] == 0){
            binary[index] = 1;
        }else{
            binary[index] = 0;
            add1ToIndex(binary, --index);
        }
    }

}
