package com.amo.codility.utils;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by ayeminoo on 5/27/17.
 */
public class BinaryArithmeticTest {
    private BinaryArithmetic binaryArithmetic;

    @Before
    public void init(){
        binaryArithmetic = new BinaryArithmetic();
    }

    @Test
    public void addOne() throws Exception {
        int[]binary = new int[]{0, 0, 1, 1};
        binaryArithmetic.addOne(binary);
        assertArrayEquals(new int[]{0, 1, 0, 0}, binary);
    }

}