package com.amo.codility.utils;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static org.junit.Assert.assertEquals;

/**
 * Created by ayeminoo on 5/26/17.
 */
public class SubsequenceTest {
    @Test
    public void getAllSubsequence() throws Exception {
    }

    @Test
    public void findShortestUncommonSequence() throws Exception {
    }

    @Test
    public void findShortestUncommonSequenceUsingLoop() throws Exception {
        for(int i = 0; i< 10; i++){
            Random random = new Random();
            int length = (int)(random.nextFloat() * 20);
            int length2 = (int)(random.nextFloat() * 20);
            String sequence = StringUtils.randomString(length);
            String sequence2 = StringUtils.randomString(length2);

            assertEquals(subsequence.findShortestUncommonSequence(sequence.toCharArray(), sequence2.toCharArray()),
                    subsequence.findShortestUncommonSequenceUsingLoop(sequence.toCharArray(), sequence2.toCharArray()));
        }
    }


    @Test
    public void findLengthOfLongestCommonSequence() throws Exception {
    }

    @Test
    public void testFindLengthOfLongestCommonSequenceGivenFirstBelongToSecond() throws Exception {
        assertEquals(3, subsequence.findLengthOfLongestCommonSequence("abc".toCharArray(), "abcd".toCharArray()));
    }

    @Test
    public void testFindLengthOfLongestCommonSequenceGivenSecondBelongToFirst() throws Exception {
        assertEquals(3, subsequence.findLengthOfLongestCommonSequence("abcd".toCharArray(), "abc".toCharArray()));
    }

    @Test
    public void testFindLengthOfLongestCommonSequenceGivenNonOverLapping(){
        assertEquals(0, subsequence.findLengthOfLongestCommonSequence("abcd".toCharArray(), "efgh".toCharArray()));
    }

    @Test
    public void testFindLengthOfLongestCommonSequence1(){
        assertEquals(2, subsequence.findLengthOfLongestCommonSequence("adef".toCharArray(), "efda".toCharArray()));
    }

    @Test
    public void testFindLengthOfLongestCommonSequence2(){
        assertEquals(3, subsequence.findLengthOfLongestCommonSequence("abcd".toCharArray(), "bcd".toCharArray()));
    }

    private Subsequence subsequence;

    @Before
    public void init(){
        subsequence = new Subsequence();
    }

    @Test
    public void testGetAllSubsequenceNumberOfSubsequenceEqualTo2PowerN() throws Exception {
        Random random = new Random();
        int length = (int)(random.nextFloat() * 20);
        String sequence = StringUtils.randomString(length);
        List<String> powerSet = subsequence.getAllSubsequence(sequence);
        assertEquals((int)Math.pow(2, length), powerSet.size());
    }

//    @Test
    public void testGetAllSubsequenceNonDuplicate(){
        Random random = new Random();
        int length = (int)(random.nextFloat() * 20);
        String sequence = StringUtils.randomString(length);
        List<String> powerSet = subsequence.getAllSubsequence(sequence);
        System.out.println(org.apache.commons.lang3.StringUtils.join(powerSet, ","));
        Map<String, Boolean> map = new HashMap<>();

        for(String p: powerSet){
            if(map.get(p) != null) {
                System.out.println(p);
                Assert.fail();
            }
            map.put(p,  true);
        }
    }
}