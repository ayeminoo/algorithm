package com.amo.codility;

import com.amo.codility.utils.Mergesort;

/**
 * Let A be a non-empty zero-indexed array consisting of N integers.

 The abs sum of two for a pair of indices (P, Q) is the absolute value |A[P] + A[Q]|, for 0 ≤ P ≤ Q < N.

 For example, the following array A:

 A[0] =  1
 A[1] =  4
 A[2] = -3
 has pairs of indices (0, 0), (0, 1), (0, 2), (1, 1), (1, 2), (2, 2).
 The abs sum of two for the pair (0, 0) is A[0] + A[0] = |1 + 1| = 2.
 The abs sum of two for the pair (0, 1) is A[0] + A[1] = |1 + 4| = 5.
 The abs sum of two for the pair (0, 2) is A[0] + A[2] = |1 + (−3)| = 2.
 The abs sum of two for the pair (1, 1) is A[1] + A[1] = |4 + 4| = 8.
 The abs sum of two for the pair (1, 2) is A[1] + A[2] = |4 + (−3)| = 1.
 The abs sum of two for the pair (2, 2) is A[2] + A[2] = |(−3) + (−3)| = 6.
 Write a function:

 class Solution { public int solution(int[] A); }

 that, given a non-empty zero-indexed array A consisting of N integers, returns the minimal abs sum of two for any pair of indices in this array.

 For example, given the following array A:

 A[0] =  1
 A[1] =  4
 A[2] = -3
 the function should return 1, as explained above.

 Given array A:

 A[0] = -8
 A[1] =  4
 A[2] =  5
 A[3] =-10
 A[4] =  3
 the function should return |(−8) + 5| = 3.

 Assume that:

 N is an integer within the range [1..100,000];
 each element of array A is an integer within the range [−1,000,000,000..1,000,000,000].
 Complexity:

 expected worst-case time complexity is O(N*log(N));
 expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
 Elements of input arrays can be modified.
 * Created by ayeminoo on 5/25/17.
 */
public class MinAbsSumOfTwo {

    public int solution(int[] A) {
        // write your code in Java SE 8
        if(A.length == 1) return Math.abs(A[0] + A[0]);
        Mergesort mergesort = new Mergesort();
        mergesort.absSort(A);
        int min = Integer.MAX_VALUE;
        for(int i = 0; i< A.length-1; i++){
            min = Math.min(min,Math.min(Math.abs(A[i] + A[i]), Math.abs(A[i]+A[i+1])));
        }
        return min;
    }

    public static void main(String[]args){
        MinAbsSumOfTwo minAbsSumOfTwo = new MinAbsSumOfTwo();
        int[] array = new int[]{1, 4, -3};
        System.out.println(1 == minAbsSumOfTwo.solution(array));
    }
}
