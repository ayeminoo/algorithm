package com.amo.codility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


/**
 *
 * We draw N discs on a plane. The discs are numbered from 0 to N − 1. A zero-indexed array A of N non-negative integers, specifying the radiuses of the discs, is given. The J-th disc is drawn with its center at (J, 0) and radius A[J].

 We say that the J-th disc and K-th disc intersect if J ≠ K and the J-th and K-th discs have at least one common point (assuming that the discs contain their borders).

 The figure below shows discs drawn for N = 6 and A as follows:

 A[0] = 1
 A[1] = 5
 A[2] = 2
 A[3] = 1
 A[4] = 4
 A[5] = 0


 There are eleven (unordered) pairs of discs that intersect, namely:

 discs 1 and 4 intersect, and both intersect with all the other discs;
 disc 2 also intersects with discs 0 and 3.
 Write a function:

 class Solution { public int solution(int[] A); }

 that, given an array A describing N discs as explained above, returns the number of (unordered) pairs of intersecting discs. The function should return −1 if the number of intersecting pairs exceeds 10,000,000.

 Given array A shown above, the function should return 11, as explained above.

 Assume that:

 N is an integer within the range [0..100,000];
 each element of array A is an integer within the range [0..2,147,483,647].
 Complexity:

 expected worst-case time complexity is O(N*log(N));
 expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
 Elements of input arrays can be modified.
 * Created by ayeminoo on 4/12/17.
 */
public class NumberOfDiscIntersections {
    public int solution(int []array){
        List<Circle> circles = new ArrayList<>(array.length);
        int count = 0;
        for(int i= 0; i< array.length ; i++){
            circles.add(new Circle(i-array[i], i+ array[i]));
        }
        Collections.sort(circles);
        System.out.println(Arrays.toString(circles.toArray()));
        for(int i =0 ;i<array.length-1;i++){
            count += findIntersectionCount(circles.get(i).right, i+1, circles);
        }
        return count;
    }

    public int findIntersectionCount(int value, int startPoint, List<Circle> circles){
        if(value< circles.get(startPoint).left) return 0;
        int lo = startPoint;
        int hi = circles.size() - 1;

        Integer lastValue = 0;
        int lastIndex = startPoint;

        while (lo <= hi) {
            int mid = (lo + hi) / 2;
            lastValue = circles.get(mid).left;
            lastIndex = mid;
            if (value < lastValue) {
                hi = mid - 1;
            } else if ( (mid+1<circles.size())? circles.get(mid+1).left < value && value > lastValue : value > lastValue) {
                lo = mid + 1;
            } else {
                break;
            }
        }
        System.out.println(lastIndex - startPoint + 1);
        return lastIndex - startPoint + 1;

    }

    class Circle implements Comparable<Circle>{
        Integer left;
        Integer right;

        Circle(Integer left, Integer right){
            this.left = left;
            this.right = right;
        }

        @Override
        public int compareTo(Circle o) {
            return this.left.compareTo(o.left);
        }

        @Override
        public String toString(){
            return left.toString();
        }
    }

    public static void main(String[]args){
        System.out.println(solution2(new int[]{1, 5, 2, 1, 4, 0}));

//        System.out.println(search(1, new int[]{0,0,2,5}));
//        System.out.println(search(3, new int[]{0,1, 2, 4, 5, 6}));
//        System.out.println(search(2, new int[]{0, 1, 3, 4, 5, 6}));


    }

    public static int intersectingDiscs(int[] A){
        int l = A.length;

        long[] A1 = new long[l];
        long[] A2 = new long[l];

        for(int i = 0; i < l; i++){
            A1[i] = (long)A[i] + i;
            A2[i] = -((long)A[i]-i);
        }

        Arrays.sort(A1);
        Arrays.sort(A2);

        System.out.println(Arrays.toString(A1));
        System.out.println(Arrays.toString(A2));
        long cnt = 0;

        for(int i = A.length - 1; i >= 0; i--){
            int pos = Arrays.binarySearch(A2, A1[i]);
            if(pos >= 0){
                while(pos < A.length && A2[pos] == A1[i]){
                    pos++;
                }
                cnt += pos;
            } else{ // element not there
                int insertionPoint = -(pos+1);
                cnt += insertionPoint;
            }

        }

        long sub = (long)l*((long)l+1)/2;
        cnt = cnt - sub;

        if(cnt > 1e7) return -1;

        return (int)cnt;
    }

    public static int solution2(int[] A) {
        // write your code in Java SE 8
        int counter = 0, j= 0;
        long[] upper = new long[A.length];
        long[] lower = new long[A.length];

        for(int i=0; i < A.length; i++) {
            lower[i] =(long) i-A[i];
            upper[i] =(long) i+A[i];
        }

        Arrays.sort(lower);
        Arrays.sort(upper);

        for(int i= 0; i<A.length; i++) {
            while(j < A.length && upper[i] >= lower[j]){
                counter += j-i;
                j++;
            }
            if(counter > 10000000) return -1;
        }

        return counter;
    }


    public static int search(int value, int[] a) {
        int lo = 0;
        int hi = a.length - 1;

        int lastValue = 0;

        while (lo <= hi) {
            int mid = (lo + hi) / 2;
            lastValue = a[mid];
            if (value < lastValue) {
                hi = mid - 1;
            } else if (a[mid+1]< value && value > lastValue) {
                lo = mid + 1;
            } else {
                return lastValue;
            }
        }
        return lastValue;
    }
}
