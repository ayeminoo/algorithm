package com.amo.codility;

/**
 * Created by ayeminoo on 5/23/17.
 */
// Java program to count number of triangles that can be
// formed from given array


import java.util.*;

/**
 * A zero-indexed array A consisting of N integers is given. A triplet (P, Q, R) is triangular if it is possible to build a triangle with sides of lengths A[P], A[Q] and A[R]. In other words, triplet (P, Q, R) is triangular if 0 ≤ P < Q < R < N and:

 A[P] + A[Q] > A[R],
 A[Q] + A[R] > A[P],
 A[R] + A[P] > A[Q].
 For example, consider array A such that:

 A[0] = 10    A[1] = 2    A[2] = 5
 A[3] = 1     A[4] = 8    A[5] = 12
 There are four triangular triplets that can be constructed from elements of this array, namely (0, 2, 4), (0, 2, 5), (0, 4, 5), and (2, 4, 5).

 Write a function:

 int solution(int A[], int N);
 that, given a zero-indexed array A consisting of N integers, returns the number of triangular triplets in this array.

 For example, given array A such that:

 A[0] = 10    A[1] = 2    A[2] = 5
 A[3] = 1     A[4] = 8    A[5] = 12
 the function should return 4, as explained above.

 Assume that:

 N is an integer within the range [0..1,000];
 each element of array A is an integer within the range [1..1,000,000,000].
 Complexity:

 expected worst-case time complexity is O(N2);
 expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
 Elements of input arrays can be modified.
 */

/**
 *
 * I think there is two main idea here
 * 1)
 * if a<= b <= c, we don't need to check all three condition as
 * a + b> c
 * b + c> a
 * a + c> b
 * we just need to check a + b > c, that is enough.
 *
 * 2)
 * we don't need to loop through z from scratch evey time y is increased by 1.
 * we can assume all previous valid condition for z is applicable for y+1 as well.
 *
 * If we get these logic, problem is solved :)
 */
class CountTriangles
{
    // Function to count all possible triangles with arr[]
    // elements
    static int findNumberOfTriangles(int arr[])
    {
        int n = arr.length;
        // Sort the array elements in non-decreasing order
        Arrays.sort(arr);

        // Initialize count of triangles
        int count = 0;

        // Fix the first element.  We need to run till n-3 as
        // the other two elements are selected from arr[i+1...n-1]
        for (int i = 0; i < n-2; ++i)
        {
            // Initialize index of the rightmost third element
            int k = i + 2;

            // Fix the second element
            for (int j = i+1; j < n; ++j)
            {
                /* Find the rightmost element which is smaller
                   than the sum of two fixed elements
                   The important thing to note here is, we use
                   the previous value of k. If value of arr[i] +
                   arr[j-1] was greater than arr[k], then arr[i] +
                   arr[j] must be greater than k, because the
                   array is sorted. */
                while (k < n && arr[i] + arr[j] > arr[k])
                    ++k;

               /* Total number of possible triangles that can be
                  formed with the two fixed elements is k - j - 1.
                  The two fixed elements are arr[i] and arr[j].  All
                  elements between arr[j+1] to arr[k-1] can form a
                  triangle with arr[i] and arr[j]. One is subtracted
                  from k because k is incremented one extra in above
                  while loop. k will always be greater than j. If j
                  becomes equal to k, then above loop will increment
                  k, because arr[k] + arr[i] is always/ greater than
                  arr[k] */
                count += k - j - 1;
            }
        }
        return count;
    }

    /*public static void main (String[] args)
    {
        int arr[] = {3, 10, 10};
        System.out.println("Total number of triangles is " +
                findNumberOfTriangles(arr));
    }*/

    public int solution(int[]data){
        int count = 0;
        Arrays.sort(data);
        for(int a = 0; a < data.length -2; a++){
            int c = a + 2;
            for(int b = a +1; b < data.length -1; b++){
                while(c < data.length && data[a]+ data[b] > data[c]){
                    c++;
                }
                count += c - b -1;
            }
        }
        return count;
    }

    public static void main(String[]args){
        CountTriangles countTriangles =  new CountTriangles();
        int []A = new int[]{10, 2, 5, 1, 8, 12};

        System.out.println(1 == countTriangles.solution(new int[]{5, 8, 10}));
        System.out.println(4 == countTriangles.solution(A));
    }

}