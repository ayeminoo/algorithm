package com.amo.codility;

/**
 * Created by ayeminoo on 3/14/17.
 */
public class NthNode {
    Node head; // head of the list

    /* Linked List node */
    class Node
    {
        int data;
        Node next;
        Node(int d)
        {
            data = d;
            next = null;
        }
    }

    void printNthFromLast(int n){
        int length = 0;
        Node tmp = head;
        while(tmp!= null){
            length++;
            tmp = tmp.next;
        }
        tmp = head;
        for(int i= 1; i< length -n; i++){
            tmp = tmp.next;
        }
        System.out.println(tmp.data);
    }

    public void push(int data){
        Node node = new Node(data);
        node.next = head;
        head = node;
    }

    public static void main(String[]args){
        NthNode llist = new NthNode();
        llist.push(20);
        llist.push(4);
        llist.push(15);
        llist.push(35);

        llist.printNthFromLast(4);
    }
}
