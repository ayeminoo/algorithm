package com.amo.codility;

/**
 * Write a function:

 class Solution { public int solution(int A, int B, int K); }

 that, given three integers A, B and K, returns the number of integers within the range [A..B] that are divisible by K, i.e.:

 { i : A ≤ i ≤ B, i mod K = 0 }

 For example, for A = 6, B = 11 and K = 2, your function should return 3, because there are three numbers divisible by 2 within the range [6..11], namely 6, 8 and 10.

 Assume that:

 A and B are integers within the range [0..2,000,000,000];
 K is an integer within the range [1..2,000,000,000];
 A ≤ B.
 Complexity:

 expected worst-case time complexity is O(1);
 expected worst-case space complexity is O(1).
 * Created by ayeminoo on 4/6/17.
 */
public class CountingDiv {
    public int solution(int a, int b, int k){
        /*1-6|2,4,6{3};
        6-1=5;5/2=3
        4-8, 4,6,8{3};
        8-4=4/2=2;

        2-21-5|5,10,15,20{4};
        21-2=19/5=3;

        19%5=4;
        21-5=16/5=3*/
        int count =1;
        if(b==0) return count;
        if(a%k!=0){
            a = ((a/k) +1)*k;
        }

        if(b>=a)return (b-a)/k + count;
        return 0;
    }
}
