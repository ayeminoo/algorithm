package com.amo.codility;

/**
 * Created by ayeminoo on 4/8/17.
 */
public class PrefixSum {
    public static int[] prefixSum(int[] input){
        int[] output = new int[input.length+1];
        output[0] = 0;
        for(int i=0; i<input.length; i++){
            output[i+1] = output[i]+ input[i];
        }
        return output;
    }
}
