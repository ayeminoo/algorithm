package com.amo.codility;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ayeminoo on 8/9/17.
 */
public class Elavator {
    public int solution(int[] A, int[] B, int M, int X, int Y) {
        int count = 0;

        Map<Integer, Boolean> destination = new HashMap<>();
        long totalWeight = 0L;
        int numberOfPeople = 0;
        for (int i = 0; i < A.length; i++) {
            if(numberOfPeople + 1 > X || totalWeight + A[i] > Y){
                //go down again
                count++;
                destination = new HashMap<>();
                totalWeight = 0L;
                numberOfPeople = 0;
            }

            if(destination.get(B[i]) == null) count++;
            destination.put(B[i], true);
            totalWeight += A[i];
            numberOfPeople++;
        }
        return ++count;
    }

    public static void main(String[] args) {
        System.out.println(new Elavator().solution(new int[]{60, 80, 40}, new int[]{2, 3, 5}, 5, 2, 200));
        System.out.println(new Elavator().solution(new int[]{40, 40, 100, 80, 20}, new int[]{3, 3, 2, 2, 3}, 3, 5, 200));

    }
}
