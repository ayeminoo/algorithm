package com.amo.codility;

/**
 * A positive integer D is a factor of a positive integer N if there exists an integer M such that N = D * M.
 * <p>
 * For example, 6 is a factor of 24, because M = 4 satisfies the above condition (24 = 6 * 4).
 * <p>
 * Write a function:
 * <p>
 * int solution(int N);
 * that, given a positive integer N, returns the number of its factors.
 * <p>
 * For example, given N = 24, the function should return 8, because 24 has 8 factors, namely 1, 2, 3, 4, 6, 8, 12, 24. There are no other factors of 24.
 * <p>
 * Assume that:
 * <p>
 * N is an integer within the range [1..2,147,483,647].
 * Complexity:
 * <p>
 * expected worst-case time complexity is O(sqrt(N));
 * expected worst-case space complexity is O(1).
 * Created by ayeminoo on 4/24/17.
 */
public class CountFactors {
    public int n2Solution(int N) {
        // write your code in Java SE 8
        int count = 0;
        for (int i = 1; i <= N; i++) {
            if (N % i == 0) count++;
        }
        return count;
    }

    public int solution85Percent(int n) {
        // write your code in Java SE 8
        if (n == 1) return 1;
        int s = 2;
        int e = n;
        int count = 2;
        while (s < e) {
            if (n % s == 0) {
                if (n / s == s) count++;
                else count += 2;
                e = (n / s);
            }
            s++;
        }
        return count;
    }

    int solution(int N) {
        int i;
        int NumFactors = 0;

        for (i = 1; (long)i * i < N; i++)
        {
            if (N % i == 0) NumFactors += 2;
        }

        if ((long)i * i == N)NumFactors++;

        return NumFactors;
    }

    public static void main(String[] args) {
        System.out.println(8 == new CountFactors().solution(24));
        System.out.println(1 == new CountFactors().solution(1));
        System.out.println(2 == new CountFactors().solution(3));
        System.out.println(new CountFactors().n2Solution(12)== new CountFactors().solution(12));
        System.out.println(3 == new CountFactors().solution(4));

    }
}
