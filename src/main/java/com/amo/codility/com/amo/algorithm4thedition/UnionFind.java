package com.amo.codility.com.amo.algorithm4thedition;

/**
 * Created by ayeminoo on 6/5/17.
 */
public interface UnionFind {
    public void union(int p, int q);
    public int find(int p);
    public boolean connected(int p, int q);
    public int count();
}
