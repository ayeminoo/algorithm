package com.amo.codility.com.amo.algorithm4thedition;

/**
 * Created by ayeminoo on 6/5/17.
 */
public class QuickFindUF implements UnionFind {

    private int[]objects;
    private int count = 0;

    public QuickFindUF(int n){
        objects = new int[n];
    }

    /**
     * This is the slow implementation of union
     * @param p
     * @param q
     */
    @Override
    public void union(int p, int q) {
        //both already exist
        if(objects[p]!=0 && objects[q]!=0 ) {
            //both equals
            if(objects[p] == objects[q])return;
            //not equal
            count--;
            int min = Math.min(objects[p], objects[q]);
            int max = Math.max(objects[p], objects[q]);

            for(int i=0;i<objects.length;i++){
                if(objects[i] == max)objects[i] = min;
                if(objects[i]>max) objects[i] = objects[i] -1;
            }
            return;
        }
        //p exist
        if(objects[p]!=0){
            objects[q] = objects[p];
            return;
        }
        //q exist
        if(objects[q]!=0){
            objects[p] = objects[q];
            return;
        }

        objects[p] = ++count;
        objects[q] = objects[p];
    }

    @Override
    public int find(int p) {
        return objects[p];
    }

    @Override
    public boolean connected(int p, int q) {
        if(objects[p]==0 || objects[q] == 0) return false;
        return objects[p]==objects[q];
    }

    @Override
    public int count() {
        return count;
    }

    /*public static void main(String[] args) throws FileNotFoundException {
        InputStream in = QuickFindUF.class.getClassLoader()
                .getResourceAsStream("tinyUF.txt");
        StdIn.setScanner(new Scanner(in));
        int n = StdIn.readInt();
        QuickFindUF uf = new QuickFindUF(n);
        while (!StdIn.isEmpty()) {
            int p = StdIn.readInt();
            int q = StdIn.readInt();
            if (uf.connected(p, q)) continue;
            uf.union(p, q);
            StdOut.println(p + " " + q);
        }
        StdOut.println(uf.count() + " components");
    }*/

    public static void main(String[]args){
        QuickFindUF unionFind = new QuickFindUF(10);

        unionFind.union(4, 3);
        unionFind.union(3, 8);
        unionFind.union(6, 5);
        unionFind.union(9, 4);
        unionFind.union(2, 1);
        System.out.println(unionFind.connected(0, 7));
        System.out.println(unionFind.connected(8, 9));
        unionFind.union(5, 0);
        unionFind.union(7, 2);
        unionFind.union(6, 1);
        unionFind.union(1, 0);
        System.out.println(unionFind.connected(0, 7));
        System.out.println(unionFind.count());

    }
}
