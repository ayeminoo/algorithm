package com.amo.codility;

import java.util.HashMap;
import java.util.Map;

class UnpairInteger {
    public int solution(int[] input) {
        // write your code in Java SE 8

        Map<Integer,  Integer> map = new HashMap<>();
        for(int i=0; i < input.length; i++){
            int value = map.getOrDefault(input[i], 0);
            map.put(input[i], ++value);
        }

        for(int i=0; i < input.length; i++){
            if(map.get(input[i]) % 2 == 1) return input[i];
        }
        return -1;
    }

    public static  void main(String[]args){
        System.out.println(new UnpairInteger().solution(new int[]{9, 3, 9, 3, 9, 7, 9, 7, 7}));
    }
}