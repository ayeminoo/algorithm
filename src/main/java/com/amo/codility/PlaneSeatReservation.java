package com.amo.codility;

/**
 * Created by ayeminoo on 8/9/17.
 */
public class PlaneSeatReservation {
    public int solution(int n, String s){
        boolean left [][] = new boolean[n][3];
        boolean middle[][] = new boolean[n][4];
        boolean right[][] = new boolean[n][3];

        for(String seatNumber : s.split("\\s+")){
            if(seatNumber.length()==0) continue;
            char seatChar = seatNumber.charAt(seatNumber.length()-1);
            String seatDigit = seatNumber.substring(0, seatNumber.length()-1);
            int digit = Integer.parseInt(seatDigit) - 1;
            //ignore invalid seat number
            if(digit < 0 || digit > n-1) continue;
            if(seatChar>='A' && seatChar <= 'C'){
                left[digit][seatChar-'A'] = true;
            }else if(seatChar >='D' && seatChar <= 'G'){
                middle[digit][seatChar - 'D'] = true;
            }else if(seatChar != 'I' && seatChar >= 'H' && seatChar <= 'K'){
                right[digit][seatChar - 'H' == 0 ? 0: seatChar - 'H' -1]=true;
            }
        }
        return getNumberOfThreeContinuousSeat(left) + getNumberOfThreeContinuousSeat(middle)
                + getNumberOfThreeContinuousSeat(right);
    }

    private int getNumberOfThreeContinuousSeat(boolean[][]seats){
        int familySeatCount = 0;
        for(int i=0;i<seats.length;i++){
            int freeSeatCount = 0;
            for(int j = 0; j <seats[i].length; j++){
                if(seats[i][j] == false) freeSeatCount++;
                else if(freeSeatCount<3) freeSeatCount=0;
            }
            if(freeSeatCount>=3) familySeatCount++;
        }
        return familySeatCount;
    }

    public static void main(String[]args){
        System.out.println(2 == new PlaneSeatReservation().solution(1, "1F"));
        System.out.println(2 == new PlaneSeatReservation().solution(1, "1E"));
        System.out.println(3 == new PlaneSeatReservation().solution(1, "1D"));
        System.out.println(3 == new PlaneSeatReservation().solution(1, " "));
        System.out.println(3 == new PlaneSeatReservation().solution(1, ""));


    }

}
