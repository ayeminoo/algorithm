package com.amo.codility;// you can also use imports, for example:
import java.util.*;

// you can write to stdout for debugging purposes, e.g.
// System.out.println("this is a debug message");

class Solution {

    public int solution(int y, String a, String b, String w){
        Calendar calendar = new Calendar(w, y);
        int from = calendar.getMonthInString().get(a);
        int to = calendar.getMonthInString().get(b);
        int total = 0;
        total += calendar.numberOfDayBefore1(a, "Monday");
        if(!a.equals(b)){
            for(int i = from +1; i<=to-1 ;i++){
                total += calendar.getDayOfMonth()[i];

            }
        }

        total += calendar.numberOfDayAfter(b, "Sunday");
        return total /  7 +1;

    }

    public static void main(String[]args){
        System.out.println(new Solution().solution(2017, "May", "June", "Sunday"));
    }

    public String solution1(String s, String t) {
        // write your code in Java SE 8
        if(s.equals(t))return "NOTHING";
        if( s.length() < t.length()-1) return "IMPOSSIBLE";
        for(int i=0; i<t.length();i++){
            if(s.charAt(i)!=t.charAt(i)){
                char c = s.charAt(i);
                if(s.length()-1 == t.length() &&
                        (s.substring(0, i) + s.substring(i+1,s.length())).equals(t)){
                    return "DELETE "+c;
                }else if(s.length() == t.length() -1 &&
                        (s.substring(0,i)+t.charAt(i)+s.substring(i,s.length())).equals(t)){
                    return "INSERT "+ t.charAt(i);
                }else if(s.length() == t.length() && i< s.length()-1 &&
                        (s.substring(0,i) + s.charAt(i+1) + s.charAt(i) + s.substring(i+2, s.length())).equals(t)){
                    return "SWAP "+ s.charAt(i) + " " + s.charAt(i+1);
                }else{
                    return "IMPOSSIBLE";
                }
            }
        }
        return "IMPOSSIBLE";
    }
}