package com.amo.codility;

import java.util.Arrays;

/**
 * Problem: You are given a non-empty, zero-indexed array A of n (1 ¬ n ¬ 100 000) integers
 a0, a1, . . . , an−1 (0 ¬ ai ¬ 1 000). This array represents number of mushrooms growing on the
 consecutive spots along a road. You are also given integers k and m (0 ¬ k, m < n).
 A mushroom picker is at spot number k on the road and should perform m moves. In
 one move she moves to an adjacent spot. She collects all the mushrooms growing on spots
 she visits. The goal is to calculate the maximum number of mushrooms that the mushroom
 picker can collect in m moves.
 For example, consider array A such that:
 2 3 7 5 1 3 9
 0 1 2 3 4 5 6
 The mushroom picker starts at spot k = 4 and should perform m = 6 moves. She might
 move to spots 3, 2, 3, 4, 5, 6 and thereby collect 1 + 5 + 7 + 3 + 9 = 25 mushrooms. This is the
 maximal number of mushrooms she can collect.
 Solution O(m2
 ): Note that the best strategy is to move in one direction optionally followed
 by some moves in the opposite direction. In other words, the mushroom picker should not
 change direction more than once. With this observation we can find the simplest solution.
 Make the first p = 0, 1, 2, . . . , m moves in one direction, then the next m − p moves in the
 opposite direction. This is just a simple simulation of the moves of the mushroom picker
 which requires O(m2
 ) time.
 Solution O(n+m): A better approach is to use prefix sums. If we make p moves in one direction,
 we can calculate the maximal opposite location of the mushroom picker. The mushroom
 picker collects all mushrooms between these extremes. We can calculate the total number of
 collected mushrooms in constant time by using prefix sums
 * Created by ayeminoo on 4/8/17.
 */
public class MushRoomPicker{

    public int solutionO_m2(int[]mushroom, int startPosition, int numberOfMoves){
        //moving right direction
        int left;
        int right;
        int largestSum = 0;
        for(int i=0; i<= numberOfMoves; i++){
            left = startPosition - (numberOfMoves - 2* i);
            right = startPosition + i;
            left = Math.max(left, 0);
            left = Math.min(startPosition, left);
            right = Math.min(right, mushroom.length -1);
            right = Math.max(startPosition, right);
            largestSum = Math.max(largestSum, findSum(mushroom, left, right));
        }

        for(int i=0; i<= numberOfMoves; i++){
            left = startPosition - i;
            right = startPosition + (numberOfMoves - 2 * i);
            left = Math.max(left, 0);
            left = Math.min(startPosition, left);
            right = Math.min(right, mushroom.length -1);
            right = Math.max(startPosition, right);
            largestSum = Math.max(largestSum, findSum(mushroom, left, right));
        }
        return largestSum;
    }


    public int solutionO_m_plus_n(int[]mushroom, int startPosition, int numberOfMoves){
        //moving right direction
        int left;
        int right;
        int largestSum = 0;
        int[]prefixSum = PrefixSum.prefixSum(mushroom);
        for(int i=0; i<= numberOfMoves; i++){
            left = startPosition - (numberOfMoves - 2* i);
            right = startPosition + i;
            left = Math.max(left, 0);
            left = Math.min(startPosition, left);
            right = Math.min(right, mushroom.length -1);
            right = Math.max(startPosition, right);
            largestSum = Math.max(largestSum, prefixSum[right+1] - prefixSum[left]);
        }

        for(int i=0; i<= numberOfMoves; i++){
            left = startPosition - i;
            right = startPosition + (numberOfMoves - 2 * i);
            left = Math.max(left, 0);
            left = Math.min(startPosition, left);
            right = Math.min(right, mushroom.length -1);
            right = Math.max(startPosition, right);
            largestSum = Math.max(largestSum, prefixSum[right+1] - prefixSum[left]);
        }
        return largestSum;
    }

    public int findSum(int[]array, int leftIndex, int rightIndex){
        assert leftIndex > 0 && leftIndex < array.length;
        assert rightIndex> 0 && rightIndex < array.length;
        assert leftIndex <= rightIndex;

        int sum = 0;
        for(int i=leftIndex; i<= rightIndex; i++){
            sum += array[i];
        }
        return sum;
    }

    public static void main(String[]args){
        int []mushRooms = new int[]{2, 3, 7, 5, 1, 3, 9};
        System.out.println("solutionO_m2 for "+ Arrays.toString(mushRooms) +" is " +
                (new MushRoomPicker().solutionO_m2(mushRooms, 4,6)== 25));

        System.out.println("solutionO_m_plus_n for "+ Arrays.toString(mushRooms) +" is " +
                (new MushRoomPicker().solutionO_m_plus_n(mushRooms, 4,6)== 25));

        mushRooms = new int[]{2, 3, 7, 5, 1, 10, 1};
        System.out.println("solutionO_m2 for "+ Arrays.toString(mushRooms) +" is " +
                (new MushRoomPicker().solutionO_m2(mushRooms, 4,5)== 26));

        mushRooms = new int[]{2, 3, 7, 5, 1, 10, 1};
        System.out.println("solutionO_m_plus_n for "+ Arrays.toString(mushRooms) +" is " +
                (new MushRoomPicker().solutionO_m_plus_n(mushRooms, 4,5)== 26));
    }

}
