package com.amo.codility;

/**
 * Created by ayeminoo on 5/26/17.
 */
public class NumberSolitaire {

    public int solution(int[] a){
        int maxInt[] = new int[a.length];
        maxInt[0] = a[0];
        maxInt[1] = a[0] + a[1];

        for(int i=2; i< a.length; i++){
            int max = Integer.MIN_VALUE;
            for(int j =1; j<=6; j++){
                if(i-j>=0){
                    max = Math.max(max, maxInt[i-j]+a[i]);
                }
            }
            maxInt[i] = max;
        }
        return maxInt[a.length-1];
    }

    public static void main(String[]args){
        NumberSolitaire t= new NumberSolitaire();
        System.out.println(8==t.solution(new int[]{1, -2, 0, 9, -1, -2}));
        System.out.println(-1 == t.solution(new int[]{1, -2}));
        System.out.println(0 == t.solution(new int[]{1, -1}));

        System.out.println(-8 == t.solution(new int[]{-2, -1, -4, -5,-6}));
        System.out.println(1 == t.solution(new int[]{1,-2, -1, -4, -5,-6, -8, 1}));
    }

}
