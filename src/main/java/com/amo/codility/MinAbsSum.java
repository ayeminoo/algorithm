package com.amo.codility;

import com.amo.codility.utils.BinaryArithmetic;

/**
 * For a given array A of N integers and a sequence S of N integers from the set {−1, 1}, we define val(A, S) as follows:

 val(A, S) = |sum{ A[i]*S[i] for i = 0..N−1 }|
 (Assume that the sum of zero elements equals zero.)

 For a given array A, we are looking for such a sequence S that minimizes val(A,S).

 Write a function:

 class Solution { public int solution(int[] A); }
 that, given an array A of N integers, computes the minimum value of val(A,S) from all possible values of val(A,S) for all possible sequences S of N integers from the set {−1, 1}.

 For example, given array:

 A[0] =  1
 A[1] =  5
 A[2] =  2
 A[3] = -2
 your function should return 0, since for S = [−1, 1, −1, 1], val(A, S) = 0, which is the minimum possible value.

 Assume that:

 N is an integer within the range [0..20,000];
 each element of array A is an integer within the range [−100..100].
 Complexity:

 expected worst-case time complexity is O(N*max(abs(A))2);
 expected worst-case space complexity is O(N+sum(abs(A))), beyond input storage (not counting the storage required for input arguments).
 Elements of input arrays can be modified.

 * Created by ayeminoo on 5/26/17.
 */
public class MinAbsSum {
    private BinaryArithmetic binaryArithmetic = new BinaryArithmetic();


    //100% correct 0% performance
    public int solution3(int[]A){
        if(A.length == 0)return 0;
        if(A.length == 1)return Math.abs(A[0]);
        int[]binary = new int[A.length];
        long min = calculate(A, binary);
        for(int i = 1; i< Math.pow(2, A.length); i++){
            binaryArithmetic.addOne(binary);
            min = Math.min(calculate(A, binary), min);
        }
        return (int) min;
    }

    private long calculate(int[]array,int[]binary){
        long sum = 0;
        for(int i=0;i<array.length; i++){
            sum += Math.abs(array[i]) * (binary[i]==0? -1:1);
        }
        return Math.abs(sum);
    }


    public int solution(int[]A){
        if(A.length == 0)return 0;
        if(A.length == 1)return Math.abs(A[0]);
        long totalSum = 0;
        for(int i=0; i<A.length;i++){
            A[i] = Math.abs(A[i]);
            totalSum += A[i];
        }
        long min = totalSum;

        for(int i= 0; i< Math.pow(2, A.length);){
            long tmp = calculate(A, i, totalSum);
            min = Math.min(tmp, min);
            min = Math.min(min, Math.abs(tmp - 2*A[A.length -1]));
            min = Math.min(min, Math.abs(tmp - 2*A[A.length - 2]));
            min = Math.min(min, Math.abs(tmp - 2*(A[A.length -1] + A[A.length - 2])));
            i+=4;
        }

        return (int)min;
    }

    long calculate(int []A, int index, long total){
        int i = 0;
        while(index >0){
            i++;
            if(index % 2 != 0){
                total -= A[A.length-i];
            }
            index /= 2;
        }
        return total;
    }

    public static void main(String[]args){
        MinAbsSum absSum = new MinAbsSum();
//        System.out.println(0==absSum.solution(new int[]{1, 5, 2, -2}));
//        System.out.println(0 == absSum.solution(new int[]{}));
        System.out.println(99 == absSum.solution(new int[]{1, -100}));
//        System.out.println(absSum.solution(new int[]{3, 3, 3, 4, 5}));

    }
}

