package com.amo.codility;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * You are going to build a stone wall. The wall should be straight and N meters long, and its thickness should be constant; however, it should have different heights in different places. The height of the wall is specified by a zero-indexed array H of N positive integers. H[I] is the height of the wall from I to I+1 meters to the right of its left end. In particular, H[0] is the height of the wall's left end and H[N−1] is the height of the wall's right end.

 The wall should be built of cuboid stone blocks (that is, all sides of such blocks are rectangular). Your task is to compute the minimum number of blocks needed to build the wall.

 Write a function:

 class Solution { public int solution(int[] H); }

 that, given a zero-indexed array H of N positive integers specifying the height of the wall, returns the minimum number of blocks needed to build it.

 For example, given array H containing N = 9 integers:

 H[0] = 8    H[1] = 8    H[2] = 5
 H[3] = 7    H[4] = 9    H[5] = 8
 H[6] = 7    H[7] = 4    H[8] = 8
 the function should return 7. The figure shows one possible arrangement of seven blocks.



 Assume that:

 N is an integer within the range [1..100,000];
 each element of array H is an integer within the range [1..1,000,000,000].
 Complexity:

 expected worst-case time complexity is O(N);
 expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
 Elements of input arrays can be modified.
 * Created by ayeminoo on 4/18/17.
 */
public class StoneWall {

    public int only50PercentCorrect(int[] h) {
        // write your code in Java SE 8
        Map<Long, Integer> map = new HashMap<>();
        long previousHeight = 0;
        boolean isUp = false;
        int numberOfCube = 0;
        for(int currentHeight: h){
            if(isUp == false && currentHeight > previousHeight){
                numberOfCube += getNumberOfCube(map);
                map = new HashMap<>();
                isUp = true;
            }else if(isUp == true && currentHeight < previousHeight){
                isUp = false;
            }
            map.put((long)currentHeight, map.get(currentHeight)== null? 1: map.get(currentHeight)+1);

            previousHeight = currentHeight;
        }
        numberOfCube += getNumberOfCube(map);
        return numberOfCube;
    }

    public int getNumberOfCube(Map<Long, Integer> map){
        int numberOfCube = 0;
        for(Long i: map.keySet()){
            numberOfCube += map.get(i) == null? 0: map.get(i);
        }
        return numberOfCube;
    }

    //100% solution
    public int solution(int[] h) {
        Stack<Long> stack = new Stack<>();
        int count = 0;
        for(long value : h){
            if(stack.size() == 0) stack.add(value);
            else{
                if(stack.peek() == value) continue;
                if(value > stack.peek()) stack.push(value);
                else if(value < stack.peek()){
                    while(true){
                        if(stack.size() != 0 && stack.peek() == value) break;
                        if(stack.size()!=0 && value < stack.peek()){
                            count ++;
                            stack.pop();
                        }else{
                            stack.push(value);
                        }
                    }
                }
            }
        }
        count += stack.size();
        return count;
    }

    public static void main(String[]args){
        System.out.println(8 == new StoneWall().solution(new int[]{2, 5, 1, 4, 6, 7, 9, 10, 1}));
        System.out.println(8 == new StoneWall().solution(new int[]{2, 5, 1, 4, 6, 7, 9, 10, 1}));

        System.out.println(3 == new StoneWall().solution(new int[]{1, 2, 3, 2, 1}));
//        System.out.println(new StoneWall().solution(new int[]{1, 2, 3, 2, 3}));

        System.out.println(4 == new StoneWall().solution(new int[]{1, 2, 3, 2, 4, 4}));
        System.out.println(4 == new StoneWall().solution(new int[]{2, 4, 3, 1}));


    }
}
