package com.amo.codility;

import java.util.Stack;

/**
 * A string S consisting of N characters is considered to be properly nested if any of the following conditions is true:

 S is empty;
 S has the form "(U)" or "[U]" or "{U}" where U is a properly nested string;
 S has the form "VW" where V and W are properly nested strings.
 For example, the string "{[()()]}" is properly nested but "([)()]" is not.

 Write a function:

 class Solution { public int solution(String S); }

 that, given a string S consisting of N characters, returns 1 if S is properly nested and 0 otherwise.

 For example, given S = "{[()()]}", the function should return 1 and given S = "([)()]", the function should return 0, as explained above.

 Assume that:

 N is an integer within the range [0..200,000];
 string S consists only of the following characters: "(", "{", "[", "]", "}" and/or ")".
 Complexity:

 expected worst-case time complexity is O(N);
 expected worst-case space complexity is O(N) (not counting the storage required for input arguments).
 * Created by ayeminoo on 4/13/17.
 */
public class Brackets {
    public int solution(String S) {
        if(S.equals(""))return 1;
        Stack<Character> stack = new Stack<>();
        for(char c : S.toString().toCharArray()){
            if(c == '{' || c == '(' || c== '[') stack.push(c);
            else {
                if(stack.size()==0) return 0;
                char takenOutChar = stack.pop();
                if(takenOutChar != getOpposite(c)) return 0;
            }
        }
        return stack.size() == 0? 1:0;
    }

    public char getOpposite(char ch){
        switch (ch){
            case '}': return '{';
            case ')': return '(';
            case ']': return '[';
        }
        assert false;
        return ' ';
    }

    public static void main(String[]args){
        System.out.println(new Brackets().solution("{[()()]}"));
        System.out.println(new Brackets().solution("{[()()]}{("));
        System.out.println(new Brackets().solution("([)()]"));
        System.out.println(new Brackets().solution(")[)()]"));

    }
}
