package com.amo.codility;

import com.amo.codility.utils.ArrayUtils;

import java.util.Collections;
import java.util.List;

/**
 * A zero-indexed array A consisting of N integers is given. A triplet (P, Q, R) is triangular if 0 ≤ P < Q < R < N and:
 * <p>
 * A[P] + A[Q] > A[R],
 * A[Q] + A[R] > A[P],
 * A[R] + A[P] > A[Q].
 * For example, consider array A such that:
 * <p>
 * A[0] = 10    A[1] = 2    A[2] = 5
 * A[3] = 1     A[4] = 8    A[5] = 20
 * Triplet (0, 2, 4) is triangular.
 * <p>
 * Write a function:
 * <p>
 * class Solution { public int solution(int[] A); }
 * <p>
 * that, given a zero-indexed array A consisting of N integers, returns 1 if there exists a triangular triplet for this array and returns 0 otherwise.
 * <p>
 * For example, given array A such that:
 * <p>
 * A[0] = 10    A[1] = 2    A[2] = 5
 * A[3] = 1     A[4] = 8    A[5] = 20
 * the function should return 1, as explained above. Given array A such that:
 * <p>
 * A[0] = 10    A[1] = 50    A[2] = 5
 * A[3] = 1
 * the function should return 0.
 * <p>
 * Assume that:
 * <p>
 * N is an integer within the range [0..100,000];
 * each element of array A is an integer within the range [−2,147,483,648..2,147,483,647].
 * Complexity:
 * <p>
 * expected worst-case time complexity is O(N*log(N));
 * expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
 * Elements of input arrays can be modified.
 * Created by ayeminoo on 4/9/17.
 */
public class Triangle {
    public int solution(int[] A) {
        if (A.length < 3) return 0;
        // write your code in Java SE 8
        List<Long> list = ArrayUtils.toLongList(A);
        Collections.sort(list);
        int P, Q, R;
        for (int i = 0; i <= A.length - 3; i++) {
            P = i;
            Q = i + 1;
            R = i + 2;
            if (list.get(P) + list.get(Q) > list.get(R) &&
                    list.get(Q) + list.get(R) > list.get(P) &&
                    list.get(R) + list.get(P) > list.get(Q))
                return 1;
        }

        return 0;
    }

    public static void main(String[] args) {
        System.out.println(new Triangle().solution(new int[]{10, 2, 5, 1, 8, 20}));
    }
}
