package com.amo.codility;

import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ayeminoo on 6/10/17.
 */

class Calendar {
    private String[]months = {"January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"};

    private Map<String, Integer> monthInString = new HashMap<>();

    private int []dayOfMonth = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    public int[] getDayOfMonth() {
        return dayOfMonth;
    }

    private String []firstDayOfMonth = new String[12];

    private String[] daysInString = {"Monday", "Tuesday", "Wednesday", "Thursday",
    "Friday", "Saturday", "Sunday"};

    public Map<String, Integer> getMonthInString() {
        return monthInString;
    }

    private Map<String, Integer> days = new HashMap<>();

    public Calendar(String dayOfWeek, int year){
        days.put("Monday", 1);
        days.put("Tuesday", 2);
        days.put("Wednesday", 3);
        days.put("Thursday", 4);
        days.put("Friday", 5);
        days.put("Saturday", 6);
        days.put("Sunday", 7);

        for(int i = 0; i<12; i++){
            monthInString.put(months[i], i+1);
        }

        firstDayOfMonth[0] = dayOfWeek;
        for(int i=1;i<12;i++){
            int extraDay  = (i==1 && (year % 4) == 0 )? (dayOfMonth[i]+1) % 7 : dayOfMonth[i-1] % 7;
            firstDayOfMonth[i] = daysInString[(days.get(firstDayOfMonth[i-1])+ extraDay -1)%7];
        }
    }

    public String[] getFirstDayOfMonth(){
        return firstDayOfMonth;
    }

    public int numberOfDayBefore(String month, String dayOfWeek){
        int a = days.get(dayOfWeek);
        int b = days.get(firstDayOfMonth[monthInString.get(month) -1]);
         return  (7 - Math.abs(a-b))%7;
    }

    public int numberOfDayBefore1(String month, String daOfWeek){
        int m  = dayOfMonth[monthInString.get(month) -1];
        return m - numberOfDayBefore(month, daOfWeek);
    }

    public int numberOfDayAfter(String month, String dayOfWeek){
        int m = (monthInString.get(month)-1) % 12;
        return dayOfMonth[m] - ((7 - numberOfDayBefore(months[monthInString.get(month) % 12], dayOfWeek)));
    }

}
