package com.amo.codility;

import com.amo.codility.utils.GCD;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ayeminoo on 5/10/17.
 */
public class CommonPrimeDivisors {

    //92 percent but fail with maximum numbers
    public int solution(int[] A, int[] B) {
        // write your code in Java SE 8
        int count = 0;
        for(int i = 0; i<A.length;i++){
            if(hasSamePrimeDivisor(A[i], B[i])) count++;
        }
        return count;
    }

    private List<Integer> primes = new ArrayList<>();
    private int lastIndex = 1;
    private int max = 1;


    //correct but not good in performance
    public int solution2(int[] A, int[] B) {
        // write your code in Java SE 8
        int count = 0;
        for(int i = 0; i<A.length;i++){
            if(isPrimeDivisorSame(A[i], B[i])) count++;
        }
        return count;
    }

    private boolean isPrimeDivisorSame(int x, int y){
        calculatePrime(Math.max(x, y));
        for(int prime: primes){
            if((x % prime == 0 && y % prime != 0) || (y % prime == 0 && x % prime != 0)) return false;
        }
        return true;
    }

    private void calculatePrime(int max){
        if(max > this.max){
            for(; this.lastIndex <= max; this.lastIndex++){
                if(isPrime(this.lastIndex))primes.add(this.lastIndex);
            }
            this.max = max;
        }
    }

    private boolean isPrime(int number){
        if(number < 2) return false;
        int i = 2;
        while((long)i*i <=number){
            if(number %i ==0) return false;
            i++;
        }
        return true;
    }
    public static void main(String[]args){
        CommonPrimeDivisors c = new CommonPrimeDivisors();
//        c.calculatePrime(10);
//        c.calculatePrime(20);
//        System.out.println(c.solution(new int[]{15,10,3}, new int[]{75, 30, 5}));
//        System.out.println(c.solution(new int[]{2, 1, 2}, new int[]{1, 2, 2}));

        System.out.println(GCD.byDivision(12, 90));
        System.out.println(GCD.byDivision(15, 75));
        System.out.println(GCD.byDivision(2, 1));
        System.out.println(GCD.byDivision(4, 18));

        System.out.println("------------");
        System.out.println(true == c.hasSamePrimeDivisor(15, 75)); // gcd is one of number
        System.out.println(true == c.hasSamePrimeDivisor(75, 15)); // gcd is one of number
        System.out.println(true == c.hasSamePrimeDivisor(12, 18)); // 2,2, 3 and 2, 3, 3
        System.out.println(true == c.hasSamePrimeDivisor(24, 18)); // 2,2, 2, 3 and 2, 3, 3

        System.out.println(false == c.hasSamePrimeDivisor(10, 30)); // 2, 5 and 2, 3,
        System.out.println(false == c.hasSamePrimeDivisor(5, 15)); // 5 and 3, 5
        System.out.println(false == c.hasSamePrimeDivisor(5, 10)); // 5 and 3, 5

        System.out.println(false == c.hasSamePrimeDivisor(2, 1)); // 5 and 3, 5

        System.out.println(false == c.hasSamePrimeDivisor(3, 2)); // 5 and 3, 5

    }


    public boolean hasSamePrimeDivisor(int x, int y){
        int i = 2;

        if(x ==1 ^ y == 1) return false;
        while (i <= Math.sqrt(Math.max(x, y))){
            if((x % i == 0 && y % i != 0) || (y % i == 0 && x % i != 0) ) return false;
            if(x % i == 0){
                int guess = (int)(Math.log(x) / Math.log(i)); // use guess to optimize the very large number case
                while(x % Math.pow(i, guess) != 0){guess--;}
                x = x / (int)Math.pow(i, guess);
            }
            if(y % i == 0){
                int guess = (int)(Math.log(y) / Math.log(i)); // use guess to optimize the very large number case
                while(y % Math.pow(i, guess) != 0){guess--;}
                y = y / (int)Math.pow(i, guess);
            }
            i++;
        }


        return x == y;
    }



}
