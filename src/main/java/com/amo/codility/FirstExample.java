/*
package com.company;//STEP 1. Import required packages
import java.sql.*;

public class FirstExample {
   
   public static void main(String[] args) {
   Connection conn = null;
   Statement stmt = null;
   try{
      DriverManager.registerDriver
              (new oracle.jdbc.driver.OracleDriver());
      conn = DriverManager.getConnection
              ("jdbc:oracle:thin:@mighty.local:1521:orcl","mty","mighty123");

      //STEP 4: Execute a query
      System.out.println("Creating statement...");
      stmt = conn.createStatement();
      String sql;
      sql = "SELECT C_TERMS_AND_CONDITIONS from t_offer";
      ResultSet rs = stmt.executeQuery(sql);

      //STEP 5: Extract data from result set
      while(rs.next()){
         //Retrieve by column name
         String description = rs.getString("C_TERMS_AND_CONDITIONS");


         //Display values
         System.out.println("C_TERMS_AND_CONDITIONS: " + description);
      }
      //STEP 6: Clean-up environment
      rs.close();
      stmt.close();
      conn.close();
   }catch(SQLException se){
      //Handle errors for JDBC
      se.printStackTrace();
   }catch(Exception e){
      //Handle errors for Class.forName
      e.printStackTrace();
   }finally{
      //finally block used to close resources
      try{
         if(stmt!=null)
            stmt.close();
      }catch(SQLException se2){
      }// nothing we can do
      try{
         if(conn!=null)
            conn.close();
      }catch(SQLException se){
         se.printStackTrace();
      }//end finally try
   }//end try
   System.out.println("Goodbye!");
}//end main
}//end FirstExample*/
