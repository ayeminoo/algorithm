package com.amo.codility;

import java.util.Stack;

/**
 * Created by ayeminoo on 3/14/17.
 */
public class PostOrder {

    Stack<Integer> stack = new Stack<>();
    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode(int x) { val = x; }
    }

    public void postOrder(TreeNode root){
        if(root != null) stack.add(root.val);
        if(root.right!= null)postOrder(root.right);
        if(root.left != null) postOrder(root.left);
    }

    public void print(){
        TreeNode a = new TreeNode(1);
        a.val =1 ;
        TreeNode b = new TreeNode(2);
        a.left = b;
        TreeNode c= new TreeNode(3);
        a.right = c;

        TreeNode d= new TreeNode(4);
        b.left= d;

        TreeNode e= new TreeNode(5);
        b.right = e;
        TreeNode f = new TreeNode(6);
        e.left = f;

        postOrder(a);

        while(stack.size()!= 0){
            System.out.println(stack.pop());
        }

    }

    public static void main(String[]args){

        new PostOrder().print();
    }
}
