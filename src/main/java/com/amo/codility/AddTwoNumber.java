
package com.amo.codility;
 /*
 *
 * You are given two non-empty linked lists representing two non-negative integers. The digits are stored in reverse order and each of their nodes contain a single digit. Add the two numbers and return it as a linked list.
 *
 * You may assume the two numbers do not contain any leading zero, except the number 0 itself.
 *
 *       Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
 *       Output: 7 -> 0 -> 8

*/

public class AddTwoNumber {

    public static ListNode addTwoNumber(ListNode number1 , ListNode number2){
        ListNode digit1 = number1;
        ListNode digit2 = number2;
        int sum = 0;
        ListNode answer = null;
        ListNode root = null;
        while(digit1 != null || digit2 != null ){
            if(digit1 != null){
                sum += digit1.val;
                digit1 = digit1.next;
            }
            if(digit2 != null){
                sum += digit2.val;
                digit2 = digit2.next;
            }

            ListNode tmp = new ListNode(sum % 10);
            sum /= 10;
            if(answer == null) {
                answer = tmp;
                root = answer;
            }else{
                answer.next = tmp;
                answer = answer.next;
            }
        }
        return root;
    }

    public static void main(String[]args){
        ListNode first = new ListNode(2);
        ListNode second = new ListNode(5);

        ListNode firstPointer = first, secondPointer = second;
        firstPointer.next = new ListNode(4);
        firstPointer = firstPointer.next;
        secondPointer.next = new ListNode(6);
        secondPointer = secondPointer.next;
        firstPointer.next = new ListNode(3);
        firstPointer = firstPointer.next;
        secondPointer.next = new ListNode(4);
        secondPointer = secondPointer.next;

        ListNode answer = addTwoNumber(first, second);

        while(answer != null){
            System.out.print(answer.val + " -> ");
            answer = answer.next;
        }
    }
}

class ListNode {
      int val;
      ListNode next;
      ListNode(int x) { val = x; }
}

