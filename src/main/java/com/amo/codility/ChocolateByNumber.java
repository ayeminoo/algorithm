package com.amo.codility;

import com.amo.codility.utils.GCD;

/**
 * Created by ayeminoo on 5/10/17.
 */
public class ChocolateByNumber {

    // you can also use imports, for example:
// import java.util.*;

// you can write to stdout for debugging purposes, e.g.
// System.out.println("this is a debug message");

    class Solution {
        public int solution(int N, int M) {
            int gcd =  GCD.byDivision(N, M);
            return N / gcd;
        }

    }
}
