package com.amo.codility;

/**
 * Created by ayeminoo on 6/4/17.
 */
public class Graph {
    public boolean isIntersect(double x1, double y1, double x2, double y2,
                               double xx1, double yy1, double xx2, double yy2){
        double m = (y2- y1)/(x2 -x1);
        double mm = (yy2 - yy1)/(xx2 - xx1);

        if(m == mm) return false;//parallel lines

        double x = (-1 * m * x1 + y1 + mm * xx1 - yy1) /(mm - m);
        double y = m * ( x - x1) + y1;

        if(Math.min(x1, x2) <= x && x <= Math.max(x1, x2)
                && Math.min(y1, y2) <= y && y <= Math.max(y1, y2)
                && Math.min(xx1, xx2) <=x && x <= Math.max(xx1, xx2)
                && Math.min(yy1, yy2) <=y && y <= Math.max(yy1, yy2))return true;
        return false;
    }

    public static void main(String[]args){
        Graph g = new Graph();

        System.out.println(g.isIntersect(0, 1, 4, 3, 1, 3, 2, 1));
        System.out.println(g.isIntersect(0, 1, 4, 3, 3, 2, 5, 1));


    }
}
