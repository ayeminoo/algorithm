package com.amo.codility;

/**
 * A non-empty zero-indexed array A consisting of N integers is given.

 A triplet (X, Y, Z), such that 0 ≤ X < Y < Z < N, is called a double slice.

 The sum of double slice (X, Y, Z) is the total of A[X + 1] + A[X + 2] + ... + A[Y − 1] + A[Y + 1] + A[Y + 2] + ... + A[Z − 1].

 For example, array A such that:

 A[0] = 3
 A[1] = 2
 A[2] = 6
 A[3] = -1
 A[4] = 4
 A[5] = 5
 A[6] = -1
 A[7] = 2
 contains the following example double slices:

 double slice (0, 3, 6), sum is 2 + 6 + 4 + 5 = 17,
 double slice (0, 3, 7), sum is 2 + 6 + 4 + 5 − 1 = 16,
 double slice (3, 4, 5), sum is 0.
 The goal is to find the maximal sum of any double slice.

 Write a function:

 class Solution { public int solution(int[] A); }

 that, given a non-empty zero-indexed array A consisting of N integers, returns the maximal sum of any double slice.

 For example, given:

 A[0] = 3
 A[1] = 2
 A[2] = 6
 A[3] = -1
 A[4] = 4
 A[5] = 5
 A[6] = -1
 A[7] = 2
 the function should return 17, because no double slice of array A has a sum of greater than 17.

 Assume that:

 N is an integer within the range [3..100,000];
 each element of array A is an integer within the range [−10,000..10,000].
 Complexity:

 expected worst-case time complexity is O(N);
 expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
 Elements of input arrays can be modified.
 * Created by ayeminoo on 4/20/17.
 */
public class MaxDoubleSliceSum {
    public int solution(int[] a) {
        // write your code in Java SE 8
        if(a.length<=3) return 0;
        int []left = new int[a.length -2];
        int []right = new int[a.length -2];
        int max = Integer.MIN_VALUE;
        left[0] = a[1];
        right[right.length-1] = a[a.length-2];
        for(int i=2; i<a.length -1; i++){
            left[i-1] = left[i-2] + a[i] > a[i]? left[i-2] + a[i]: a[i];
        }
        for(int i = a.length - 3; i>0; i--){
            right[i-1] = right[i] + a[i] > a[i]? right[i] + a[i]: a[i];
        }
//        System.out.println(Arrays.toString(left));
//        System.out.println(Arrays.toString(right));

        for(int i=0; i< left.length; i++){
            int l = i-1 < 0 ?0: left[i-1];
            int r = i+1 > left.length -1? 0: right[i+1];
            if(l < 0 )l = 0;
            if(r < 0) r = 0;
            max  = l + r > max? l + r : max;
        }
        return max;
    }

    public static void main(String[]args){
        System.out.println(30 == new MaxDoubleSliceSum().solution(new int[]{-4, -1, 4, 5, 6, -1, 4, 5, 6, -4}));
        System.out.println(0 == new MaxDoubleSliceSum().solution(new int[]{-4, -2, -1, -1, -1, -2, -4}));
        System.out.println(2 == new MaxDoubleSliceSum().solution(new int[]{1, 1, 1, -1, -4, -1, 1, 1, 1}));
        System.out.println(9 == new MaxDoubleSliceSum().solution(new int[]{1, 1, 2, 3, 4, 5}));
        System.out.println(17 == new MaxDoubleSliceSum().solution(new int[]{5, 17, 0, 3}));

        System.out.println(10 == new MaxDoubleSliceSum().solution(new int[]{0, 10, -5, -2, 0}));

    }
}
