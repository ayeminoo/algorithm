package com.amo.codility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 A non-empty zero-indexed array A consisting of N integers is given.

 The leader of this array is the value that occurs in more than half of the elements of A.

 An equi leader is an index S such that 0 ≤ S < N − 1 and two sequences A[0], A[1], ..., A[S] and A[S + 1], A[S + 2], ..., A[N − 1] have leaders of the same value.

 For example, given array A such that:

 A[0] = 4
 A[1] = 3
 A[2] = 4
 A[3] = 4
 A[4] = 4
 A[5] = 2
 we can find two equi leaders:

 0, because sequences: (4) and (3, 4, 4, 4, 2) have the same leader, whose value is 4.
 2, because sequences: (4, 3, 4) and (4, 4, 2) have the same leader, whose value is 4.
 The goal is to count the number of equi leaders.

 Write a function:

 class Solution { public int solution(int[] A); }

 that, given a non-empty zero-indexed array A consisting of N integers, returns the number of equi leaders.

 For example, given:

 A[0] = 4
 A[1] = 3
 A[2] = 4
 A[3] = 4
 A[4] = 4
 A[5] = 2
 the function should return 2, as explained above.

 Assume that:

 N is an integer within the range [1..100,000];
 each element of array A is an integer within the range [−1,000,000,000..1,000,000,000].
 Complexity:

 expected worst-case time complexity is O(N);
 expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
 Elements of input arrays can be modified
 * Created by ayeminoo on 4/19/17.
 */
public class EquiLeader {

    public static void main(String[]args){
        System.out.println(2 == new EquiLeader().solution(new int[]{4,3,4,4,4,2}));
    }

    public int solution(int[] a) {
        List<Long> first = createLeaderList(a);
        reverseArray(a);
        List<Long> second = createLeaderList(a);
        Collections.reverse(second);
        int count = 0;
        for(int i= 0; i<a.length -1 ;i++){
            if(first.get(i) != null && first.get(i).equals(second.get(i+1))) count++;
        }
        return count;
    }

    public List<Long> createLeaderList(int []a){
        Map<Long, Integer> firstMap = new HashMap<>();
        List<Long> firstA = new ArrayList<>();
        Long fMaxValue = null;

        for(int i=0; i< a.length;i++){
            Long value = new Long(a[i]);
            firstMap.put(value, firstMap.get(value)==null?1: firstMap.get(value) +1);
            if(fMaxValue == value) {
                firstA.add(value);
                continue;
            }
            else{
                int valueCount = firstMap.get(value) == null? 0: firstMap.get(value);
                int maxCount = firstMap.get(fMaxValue) == null? 0: firstMap.get(fMaxValue);
                if(maxCount>valueCount){
                    if(maxCount <= (i+1)/2){
                        firstA.add(null);
                        continue;
                    };
                    firstA.add(fMaxValue);
                }else{
                    fMaxValue = value;
                    if(valueCount <= (i+1)/2){
                        firstA.add(null);
                        continue;
                    }
                    firstA.add(value);
                }
            }

        }
        return firstA;
    }

    void reverseArray(int inputArray[])
    {

        int temp;

        for (int i = 0; i < inputArray.length/2; i++)
        {
            temp = inputArray[i];

            inputArray[i] = inputArray[inputArray.length-1-i];

            inputArray[inputArray.length-1-i] = temp;
        }

    }
}
