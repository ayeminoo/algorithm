package com.amo.codility;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Main {

    public static void main(String[] args) {
        Tree root = new Tree();
        root.x = 4;

        Tree l1 = new Tree();
        l1.x = 5;
        root.l = l1;

        Tree r1 = new Tree();
        r1.x = 6;
        root.r = r1;

        Tree l1l1 = new Tree();
        l1l1.x = 4;
        l1.l = l1l1;

        Tree r1l1 = new Tree();
        r1l1.x = 1;
        r1.l = r1l1;

        Tree r1r1 = new Tree();
        r1r1.x = 6;
        r1.r = r1r1;

        Tree l1l1l1 = new Tree();
        l1l1l1.x = 5;
        l1l1.l = l1l1l1;

        System.out.println(solution(root));

    }

    public static int solution(Tree t) {
        Set<Integer> values = new HashSet<>();
        values = solution(t, values);
        return values.size();
    }

    private static Set<Integer> solution(Tree t, Set<Integer> values) {
        if (null == t.l && null == t.r) {
            values.add(t.x);
            return values;
        }
        if (null == t.l && null != t.r) {
            return solution(t.r, values);
        }
        if (null != t.l && null == t.r) {
            return solution(t.l, values);
        }
        Set<Integer> left = solution(t.l, values);
        Set<Integer> right = solution(t.r, values);
        if (left.size() > right.size()) {
            return left;
        } else {
            return right;
        }
    }

   /*public int solution(String s){
        List<Integer> left = new ArrayList<>();
        List<Integer> right = new ArrayList<>();
        for (int i = 0; i < s.length(); i++){
            if(s.charAt(i)=='('){
                left.add(left.size()+1);
            }
        }

       for (int j = s.length(); j <=0; j--){
           if(s.charAt(j)==')'){
               right.add(right.size()+1);
           }
       }



        return -1;
    }*/
}


