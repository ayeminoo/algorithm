package com.amo.codility;

import java.util.*;

/**
 * Created by ayeminoo on 3/7/17.
 */

/*Given an array S of n integers, are there elements a, b, c in S such that a + b + c = 0? Find all unique triplets in the array which gives the sum of zero.

        Note: The solution set must not contain duplicate triplets.

        For example, given array S = [-1, 0, 1, 2, -1, -4],

        A solution set is:
        [
        [-1, 0, 1],
        [-1, -1, 2]
        ]*/
public class ThreeSum {
    //[-4 -1 -1 0 1 2

    public static Collection<int[]> getThreeSumZeroTriplets(int[] given){
        Map<Integer, int[]> map = new HashMap<>();
        Arrays.sort(given);
        for(int i=0; i < given.length ;i ++){
            if(given[i]>0 ) break;
            for(int j = i+1; j < given.length; j++){
                if(given[i] + given [j] > 0) break;
                for(int k = j+1; k < given.length; k++){
                    if(given[i] + given[j] + given[k] == 0){
                        int[] zeroArray = new int[]{given[i], given[j], given[k]};
                        int hasCode = Arrays.hashCode(zeroArray);
                        if(map.get(hasCode) == null) map.put(hasCode, zeroArray);
                    }
                    if(given[i] + given[j] + given[k] > 0) break;
                }
            }
        }
        return map.values();
    }

    public static void printTriplets(Collection<int[]> triplets){
        for(int[] triplet: triplets){
            System.out.println(Arrays.toString(triplet));
        }
    }



    public static void main(String[]args){
        Collection<int[]> triplets = getThreeSumZeroTriplets(new int[]{-1, 0, 1, 2, -1, -4});
        printTriplets(triplets);

    }
}
