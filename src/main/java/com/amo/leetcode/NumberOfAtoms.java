package com.amo.leetcode;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

/**
 * Created by ayeminoo on 11/13/17.
 */
public class NumberOfAtoms {

    private class Pair{
        public String key;
        public Integer count;
        public Pair nextPair;
        Pair(String key, Integer count){
            this.key = key;
            this.count = count;
        }
    }

    public String countOfAtoms(String formula) {
        List<String> tokens = parseAtoms(formula);
        StringBuffer atoms= new StringBuffer();
        Pair head = null;
        for(int i=0; i< tokens.size(); i++){
            Pair tmp = null;
            if(tokens.get(i).equals(")")){
                if( i < tokens.size() -1 && Character.isDigit(tokens.get(i+1).charAt(0))) {
                    remove(head, Integer.parseInt(tokens.get(i+1)));
                    i++;
                }else{
                    remove(head, 1);
                }

            }else if(  i < tokens.size() -1 && Character.isDigit(tokens.get(i+1).charAt(0))) {
                tmp = new Pair(tokens.get(i), Integer.parseInt(tokens.get(i+1)));
                i++;
            }else{
                tmp = new Pair(tokens.get(i), 1);
            }

            if(head == null){
                head = tmp;
            }else if(tmp != null){
                tmp.nextPair = head;
                head = tmp;
            }
        }

        TreeMap<String, Integer> map = new TreeMap<>();
        while(head != null){
            if(map.get(head.key)==null)
                map.put(head.key, head.count);
            else map.put(head.key, map.get(head.key) + head.count);
            head = head.nextPair;
        }

        for(String key: map.keySet()){
            atoms.append(key);
            if(map.get(key)>1){
                atoms.append(map.get(key));
            }
        }
        return atoms.toString();
    }

    private void remove(Pair head, Integer mul){

        Pair tmp = head;
        Pair previous = head;
        while(true){
            if(tmp.key.equals("(")){
                previous.nextPair = tmp.nextPair;
                break;
            }else{
                tmp.count *= mul;
                previous = tmp;
                tmp = tmp.nextPair;
            }
        }

    }

    public List<String> parseAtoms(String formula){
        int head=0;
        List<String> tokens = new ArrayList<>();
        for(int i=1;i<formula.length(); i++){
            if(Character.isLowerCase(formula.charAt(i))) continue;
            if(Character.isDigit(formula.charAt(head)) && Character.isDigit(formula.charAt(i))) continue;
            tokens.add(formula.substring(head, i));
            head=i;
        }
        tokens.add(formula.substring(head));
        return tokens;
    }
}
