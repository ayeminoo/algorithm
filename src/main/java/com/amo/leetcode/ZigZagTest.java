package com.amo.leetcode;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by ayeminoo on 11/10/17.
 */
public class ZigZagTest {
    @Test
    public void testConvert() throws Exception {
        assertEquals("PAHNAPLSIIGYIR", new ZigZag().convert("PAYPALISHIRING", 3));
    }

    @Test
    public void testFastConvert() throws Exception {
        assertEquals("PAHNAPLSIIGYIR", new ZigZag().fastConvert("PAYPALISHIRING", 3));
    }

}