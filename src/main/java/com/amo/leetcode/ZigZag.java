package com.amo.leetcode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ayeminoo on 11/10/17.
 */
public class ZigZag {
    public String fastConvert(String text, int nRow){
        if(text == null || text.length() <= nRow) return text;
        StringBuffer zigzag = new StringBuffer();
        int jump = 0;
        if(nRow - 1 >=0) jump += nRow -1;
        if(nRow -2 >=0) jump += nRow -2;
        int index = 0;
        int secondaryJump = jump;

        while(index <= nRow -1 && index < text.length()){
            int i = index;
            while(i <= text.length()-1){
                if(i<= text.length()-1) zigzag.append(text.charAt(i));
                if(index!=0 && secondaryJump!=0 && (i+secondaryJump+1)<=text.length()-1)    zigzag.append(text.charAt(i+secondaryJump+1));
                i = i + jump + 1;
            }
            index++;
            secondaryJump = secondaryJump - 2 > 0? secondaryJump -2: 0;
        }
        return zigzag.toString();
    }

    //normal converting that use two dimension array to solve but a bit slow
    public String convert(String text, int nRow){
        if(text == null || text.length() <= nRow) return text;
        List<char[]> zigzag = new ArrayList<>();
        char[]rows = new char[nRow];
        boolean isVertical = true;
        int row= 0;
        for(char c: text.toCharArray()){
            rows[row] = c;

            if(row == nRow - 1){
                isVertical = false;
            }else if(row == 0) isVertical = true;

            if(isVertical == true){
                row++;
            }else{
                zigzag.add(rows);
                rows = new char[nRow];
                if(row != 0) row--;
            }

        }
        zigzag.add(rows);
        return convert(zigzag, nRow);
    }

    private String convert(List<char[]> zigzag, int nRow){
        StringBuffer stringBuffer = new StringBuffer();
        for(int j=0; j< nRow; j++){
            for(int i=0; i< zigzag.size(); i++){
                if(zigzag.get(i)[j]!='\u0000') stringBuffer.append(zigzag.get(i)[j]);
            }
        }
        return stringBuffer.toString();
    }

    public static void main(String[]args){
        System.out.println(new ZigZag().fastConvert("AB", 1));
    }
}

