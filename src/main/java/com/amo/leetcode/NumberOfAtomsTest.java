
package com.amo.leetcode;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by ayeminoo on 11/13/17.
 */
public class NumberOfAtomsTest {
    NumberOfAtoms numberOfAtoms = new NumberOfAtoms();
    @Test
    public void countOfAtoms() throws Exception {
        assertEquals("K4N2O14S4", numberOfAtoms.countOfAtoms("K4(ON(SO3)2)2"));
        assertEquals("H2O", numberOfAtoms.countOfAtoms("H2O"));
        assertEquals("H2MgO2", numberOfAtoms.countOfAtoms("Mg(OH)2"));
    }


}