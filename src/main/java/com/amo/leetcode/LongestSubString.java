package com.amo.leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * Given a string, find the length of the longest substring without repeating characters.
 * Examples:
 *
 * Given "abcabcbb", the answer is "abc", which the length is 3.
 * Given "bbbbb", the answer is "b", with the length of 1.
 * Given "pwwkew", the answer is "wke", with the length of 3.
 * Note that the answer must be a substring, "pwke" is a subsequence and not a substring.
 *
 * Created by ayeminoo on 11/14/17.
 */
public class LongestSubString {
    public int lengthOfLongestSubstring2(String s) {
        int maxCount = 0;
        Map<Character, Integer> map = new HashMap<>();
        int index = 0;
        int fi = 0;
        int count = 0;
        while(index < s.length()){
            Character c = s.charAt(index);
            if(map.get(c) == null || map.get(c)< fi){
                count++;
            }else{
                maxCount = maxCount> count? maxCount: count;
                fi = map.get(c) + 1;
                count = index - fi + 1;
            }
            map.put(c, index);
            index++;
        }
        return maxCount> count? maxCount: count;
    }

    //this code is faster because it does not use HashMap instead it use primitive array
    public int lengthOfLongestSubstring(String s) {
        int maxCount = 0;
        int[] charIndex = new int[256];
        int index = 0;
        int fi = 1;
        int count = 0;
        while(index < s.length()){
            char c = s.charAt(index);
            if(charIndex[c] < fi){
                count++;
            }else{
                maxCount = maxCount> count? maxCount: count;
                fi = charIndex[c] + 1;
                count = index - fi + 2;
            }
            charIndex[c] = index + 1;
            index++;
        }
        return maxCount> count? maxCount: count;
    }
}
