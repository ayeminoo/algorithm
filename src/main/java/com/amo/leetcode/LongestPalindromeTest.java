package com.amo.leetcode;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by ayeminoo on 11/14/17.
 */
public class LongestPalindromeTest {
    private LongestPalindrome palindrome = new LongestPalindrome();

    @Test
    public void testLongestPalindrome(){
        assertEquals("bb", palindrome.longestPalindrome("cbbd"));
        assertEquals("bab", palindrome.longestPalindrome("babad"));
        assertEquals("bbb", palindrome.longestPalindrome("bbb"));
        assertEquals("a", palindrome.longestPalindrome("a"));
        assertEquals("aa", palindrome.longestPalindrome("aa"));
        assertEquals("ccbaddabcc", palindrome.longestPalindrome("abccbaddabcc"));
        assertEquals("a", palindrome.longestPalindrome("abcda"));
    }

}