package com.amo.leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * You may assume that each input would have exactly one solution,
 * and you may not use the same element twice.
 * Created by ayeminoo on 11/13/17.
 */
public class TwoSum {
    public int[] twoSum(int[] nums, int target) {
        if (nums == null) return null;
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            Integer firstIndex = map.get(target - nums[i]);
            if (firstIndex == null)
                map.put(nums[i], i);
            else
                return new int[]{firstIndex, i};
        }
        return null;
    }
}
