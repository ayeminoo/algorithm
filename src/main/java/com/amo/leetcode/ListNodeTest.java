package com.amo.leetcode;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by ayeminoo on 11/14/17.
 */
public class ListNodeTest {
    @Test
    public void createListNode() throws Exception {
        ListNode firstDigit = new ListNode(1);
        ListNode secondDigit = new ListNode(2);
        secondDigit.next = firstDigit;

        ListNode head = ListNode.createListNode(12);

        assertEquals(secondDigit.val, head.val);
        secondDigit = secondDigit.next;
        head = head.next;
        assertEquals(secondDigit.val, head.val);
    }

    @Test
    public void toNumber() throws Exception {
        ListNode head = ListNode.createListNode(9999999991L);
        assertEquals(9999999991L, head.toNumber());
    }

}