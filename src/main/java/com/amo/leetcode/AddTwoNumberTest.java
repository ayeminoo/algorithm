package com.amo.leetcode;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by ayeminoo on 11/14/17.
 */
public class AddTwoNumberTest {

    private AddTwoNumber addTwoNumber = new AddTwoNumber();
    /**(2 -> 4 -> 3) + (5 -> 6 -> 4
 * Output: 7 -> 0 -> 8
 **/
    @Test
    public void addTwoNumbers() throws Exception {
        ListNode sum  = addTwoNumber.addTwoNumbers(ListNode.createListNode(342), ListNode.createListNode(465));
        assertEquals(807, sum.toNumber());

        sum = addTwoNumber.addTwoNumbers(ListNode.createListNode(5), ListNode.createListNode(5));
        assertEquals(10, sum.toNumber());

        sum = addTwoNumber.addTwoNumbers(ListNode.createListNode(9), ListNode.createListNode(9999999991L));
        assertEquals(10000000000L, sum.toNumber());

        sum = addTwoNumber.addTwoNumbers(ListNode.createListNode(9), ListNode.createListNode(22));
        assertEquals(31, sum.toNumber());
    }
}