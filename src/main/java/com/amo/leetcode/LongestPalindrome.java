package com.amo.leetcode;

/**
 * Given a string s, find the longest palindromic substring in s.
 * You may assume that the maximum length of s is 1000.
 *
 * Created by ayeminoo on 11/14/17.
 */
public class LongestPalindrome {
    public String longestPalindrome(String s) {
        if(s == null || s.equals("")) return null;
        if(s.length() == 1) return s;

        int maxLength = 0;
        int firstIndex = -1;
        int lastIndex = -1;

        for(int i= 1; i < s.length(); i++){
            if(s.charAt(i)==s.charAt(i-1)){
                //case 1
                int[]edges = findEdges(i-1, i, s);
                if(edges[1] - edges[0]+1 > maxLength){
                    maxLength = edges[1] - edges[0]+1;
                    firstIndex = edges[0];
                    lastIndex = edges[1];
                }
            }
            if(i+1 < s.length() && s.charAt(i-1) == s.charAt(i+1)){
                //case 2
                int[]edges = findEdges(i-1, i+1, s);
                if(edges[1] - edges[0]+1 > maxLength){
                    maxLength = edges[1] - edges[0]+1;
                    firstIndex = edges[0];
                    lastIndex = edges[1];
                }
            }

        }
        return firstIndex != -1? s.substring(firstIndex, lastIndex + 1): s.substring(0, 1);
    }

    private int[] findEdges(int l, int r, String s){
        while(l-1>=0 && r+1 < s.length() && s.charAt(l-1) == s.charAt(r+1)){
            l--;
            r++;
        }
        return new int[]{l, r};
    }
}
