package com.amo.leetcode;

public class ListNode {
    int val;
    ListNode next;

    ListNode(int x) {
        val = x;
    }

    public static ListNode createListNode(long number) {
        String numberString = Long.toString(number);
        ListNode head = new ListNode(Character.getNumericValue(numberString.charAt(0)));
        for (int i = 1; i < numberString.length(); i++) {
            ListNode node = new ListNode(Character.getNumericValue(numberString.charAt(i)));
            node.next = head;
            head = node;
        }
        return head;
    }

    public long toNumber() {
        long mul = 1;
        ListNode head = this;
        long sum = 0;
        while (head != null) {
            sum += head.val * mul;
            mul *= 10;
            head = head.next;
        }
        return sum;
    }
}