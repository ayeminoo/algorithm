package com.amo.leetcode;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by ayeminoo on 11/14/17.
 */
public class LongestSubStringTest {

    private LongestSubString longestSubString = new LongestSubString();

    @Test
    public void lengthOfLongestSubstring() throws Exception {
        assertEquals(6, longestSubString.lengthOfLongestSubstring("abcdecjhij"));
        assertEquals(3, longestSubString.lengthOfLongestSubstring("abcabcbb"));
        assertEquals(1, longestSubString.lengthOfLongestSubstring("bbbbb"));
        assertEquals(3, longestSubString.lengthOfLongestSubstring("pwwkew"));
        assertEquals(4, longestSubString.lengthOfLongestSubstring("wwwwbegg"));
        assertEquals(1, longestSubString.lengthOfLongestSubstring("a"));
        assertEquals(6, longestSubString.lengthOfLongestSubstring("abcabcabcdef"));
        assertEquals(0, longestSubString.lengthOfLongestSubstring(""));
    }

}