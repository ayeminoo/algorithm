package com.amo.leetcode;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * You may assume that each input would have exactly one solution,
 * and you may not use the same element twice.
 * Created by ayeminoo on 11/14/17.
 */
public class TwoSumTest {
    private TwoSum twoSum = new TwoSum();

    @Test
    public void testTwoSum() throws Exception {
        assertArrayEquals(new int[]{0,1}, twoSum.twoSum(new int[]{2,7,11,15}, 9));
    }

    @Test
    public void testTwoSumGivenNullArrayReturnNull(){
        assertNull(twoSum.twoSum(null, 1));
    }

    @Test
    public void testTwoSumGivenNoSummableInputReturnNull(){
        assertNull(twoSum.twoSum(new int[]{2,7,11,15}, 2));
    }

    @Test
    public void testTwoSumGivenDuplicatedData(){
        assertArrayEquals(new int[]{0,1}, twoSum.twoSum(new int[]{2,2,11,15}, 4));
    }

    @Test
    public void testTwoSumGivenLargerPositiveAndSmallerNegative(){
        assertArrayEquals(new int[]{0,1}, twoSum.twoSum(new int[]{2,-1,11,15}, 1));
    }

    @Test
    public void testTwoSumGivenLargerNegativeAndSmallerPositive(){
        assertArrayEquals(new int[]{0,1}, twoSum.twoSum(new int[]{2,-3,11,15}, -1));
    }

    @Test
    public void testTwoSumReturnNonDuplicatedIndex(){
        assertArrayEquals(new int[]{1, 2}, twoSum.twoSum(new int[]{3, 2, 4}, 6));
    }
}