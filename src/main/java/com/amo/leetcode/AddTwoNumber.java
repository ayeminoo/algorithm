package com.amo.leetcode;

/**
 * You are given two non-empty linked lists representing two non-negative integers.
 * The digits are stored in reverse order and each of their nodes contain a single digit.
 * Add the two numbers and return it as a linked list.
 * You may assume the two numbers do not contain any leading zero,
 * except the number 0 itself.
 * Input: (2 -> 4 -> 3) + (5 -> 6 -> 4
 * Output: 7 -> 0 -> 8
 * <p>
 * Created by ayeminoo on 11/14/17.
 */

public class AddTwoNumber {
    public ListNode addTwoNumbers(ListNode number1, ListNode number2) {
        int carry = 0;
        int val1 = 0;
        int val2 = 0;
        ListNode head = null;
        ListNode tail = null;
        while (number1 != null || number2 != null) {
            val1 = number1 == null ? 0 : number1.val;
            val2 = number2 == null ? 0 : number2.val;
            int result = val1 + val2 + carry;
            carry = result >= 10 ? 1 : 0;
            result = result % 10;
            if (head == null) {
                head = new ListNode(result);
                tail = head;
            } else {
                ListNode tmp = new ListNode(result);
                head.next = tmp;
                head = tmp;
            }
            if (number1 != null) number1 = number1.next;
            if (number2 != null) number2 = number2.next;
        }

        if (carry == 1) {
            ListNode tmp = new ListNode(1);
            head.next = tmp;
            head = tmp;
        }

        return tail;
    }
}

