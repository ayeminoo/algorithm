package com.amo.utility;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by ayeminoo on 1/5/18.
 */
public class BitUtilityTest {
    @Test
    public void toBitArray() throws Exception {
        boolean[] bits = new boolean[16];
        bits[13] = true;
        bits[15] = true;
        assertArrayEquals(BitUtility.toBitArray(5, 16),(bits));
    }

    @Test
    public void toBytes() throws Exception {
        boolean[] bits = new boolean[]{true, false, false,false,false,false,false,true,
                true, false, false,false,false,false,true,true
        };
        byte[] bytes = BitUtility.toBytes(bits);
        assertTrue(bytes[0] == -127);
        assertTrue(bytes[1] == (byte) 0b10000011);
    }

}